#!/usr/bin python
# -*- coding: utf-8 -*-
'''
Created on Jul 19, 2018

@author: Wei
'''
import os, sys, re, docx, subprocess, time, random, math
from PyPDF2 import PdfFileReader
import unicodedata
import difflib
from docx.api import Document
from datetime import date, datetime, timedelta

from reportlab.platypus import Flowable
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_RIGHT, TA_CENTER
from reportlab.lib.pagesizes import letter, landscape
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle, Spacer, PageBreak, Image
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch, mm
from reportlab.lib import utils
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart
from reportlab.graphics import renderPDF
from time import sleep

# import threading, functools

# def pdf2img(filename):
#     pdf = file(filename, "rb").read()
#     
#     startmark = "\xff\xd8"
#     startfix = 0
#     endmark = "\xff\xd9"
#     endfix = 2
#     i = 0
#     
#     jpgfile = file("jpgfile.jpg", "wb")
#     njpg = 0
#     while True:
#         istream = pdf.find("stream", i)
#         if istream < 0:
#             break
#         istart = pdf.find(startmark, istream, istream+20)
#         if istart < 0:
#             i = istream+20
#             continue
#         iend = pdf.find("endstream", istart)
#         if iend < 0:
#             raise Exception("Didn't find end of stream!")
#         iend = pdf.find(endmark, iend-20)
#         if iend < 0:
#             raise Exception("Didn't find end of JPG!")
#          
#         istart += startfix
#         iend += endfix
#         print("JPG %d from %d to %d" % (njpg, istart, iend))
#         jpg = pdf[istart:iend]
#         jpgfile.write(jpg)
#          
#         njpg += 1
#         i = iend
#     
#     jpgfile.close()
        
class Utility(object):
    
#     # A decorator that will run its wrapped function in a new thread
#     @staticmethod
#     def run_in_other_thread(function, args):
#         # functool.wraps will copy over the docstring and some other metadata
#         # from the original function
#         @functools.wraps(function)
#         def fn_(*args, **kwargs):
#             thread = threading.Thread(target=function, args=args, kwargs=kwargs)
#             thread.start()
#             thread.join()
#         return fn_

    @staticmethod
    def s2float(s):
        try:
            f = float(s)
        except:
            f = None
        
        return f  
    
    @staticmethod
    def remove_nonascii(s):
        return ''.join([i if ord(i)<128 else ' ' for i in s])
        
    @staticmethod
    def clean_string_unicode(s0):
        uni2ascii = {ord('\xe2\x80\x94'.decode('utf-8')):ord('-'),
                    ord('\xe2\x80\x9c'.decode('utf-8')):ord('"'),
                    ord('\xe2\x80\x99'.decode('utf-8')):ord("'"),
                    ord('\xe2\x80\x98'.decode('utf-8')):ord("'"),
                    ord('\xe2\x80\x9d'.decode('utf-8')):ord('"')
            }
        if isinstance(s0, str):
            s0 = s0.decode('utf-8', 'ignore') #.replace(u'\xb1', '+-')
        s = s0.translate(uni2ascii)
        ss = unicodedata.normalize('NFKD', s).encode('ascii', 'ignore')
        ss = Utility.remove_nonascii(ss)
        return ss
    
    @staticmethod
    def pdf2text_no_OCR(filename):
        text = ''
        with open(filename, 'rb') as f:
            pdf = PdfFileReader(f)
            number_of_pages = pdf.getNumPages()

            for i in range(number_of_pages):
                page = pdf.getPage(i)
                text += '\n' + page.extractText()
        
        text = str(text)
        text_lines = text.split('\n')
        return text_lines
    
    @staticmethod
    def pdf2text(filename):
        jpgfilenames = Utility.pdf2img(filename)
        text_lines = Utility.img2txt(jpgfilenames)
        
        pos = filename.rfind('.')
        txtname = filename[:pos] + '.txt'
        f = file(txtname, "wb")
        for line in text_lines:
            f.write(line)
        f.close()
        
        return text_lines
        
    @staticmethod
    def pdf2img(filename):
        pdf = file(filename, "rb").read()
        
        startmark = "\xff\xd8"
        startfix = 0
        endmark = "\xff\xd9"
        endfix = 2
        i = 0
        
        jpgfiles = []
        njpg = 0
        while True:
            istream = pdf.find("stream", i)
            if istream < 0:
                break
            istart = pdf.find(startmark, istream, istream+20)
            if istart < 0:
                i = istream+20
                continue
            iend = pdf.find("endstream", istart)
            if iend < 0:
                raise Exception("Didn't find end of stream!")
            iend = pdf.find(endmark, iend-20)
            if iend < 0:
                raise Exception("Didn't find end of JPG!")
             
            istart += startfix
            iend += endfix
            print("JPG %d from %d to %d" % (njpg, istart, iend))
            jpg = pdf[istart:iend]
            jfname = "jpg%d.jpg" % njpg
            jpgfile = file(jfname, "wb")
            jpgfile.write(jpg)
            jpgfile.close()
            jpgfiles.append(jfname)
             
            njpg += 1
            i = iend
        
        return jpgfiles
    
    @staticmethod
    def img2txt(filenames):
        
        textfiles = []
        text = ''
        for fname in filenames:
            pos = fname.rfind('.')
            txtname = fname[:pos]
            textfiles.append(txtname + '.txt')
            cmd = "./ocr/tesseract.exe "+ fname + " " + txtname
            subprocess.Popen(cmd)
        
        ##: Wait till all jpg files are converted
        maxwait = 60
        t = 0
        while t < maxwait:
            allconverted = True
            for fn in textfiles:
                if not os.path.exists(fn):
                    allconverted = False
                    break
            if allconverted:
                break
            time.sleep(1)
            t += 1
        
        text_lines = []
        for fname in textfiles:
            try:
                with open(fname, "r") as f:
                    lines = f.readlines()
                    text_lines += lines
                    
            except Exception as e:
                print(str(e))
                sys.exit()
            
        for fname in filenames:
            os.remove(fname)
            
        for fname in textfiles:
            os.remove(fname)
            
        return text_lines
        
class ReportParser:
    def __init__(self):
        return
    
    def parse_file(self, filename):
        doc = docx.Document(filename)
#         fullText = []
#         for para in doc.paragraphs:
#             fullText.append(para.text)
        
        lines = []
        for para in doc.paragraphs:
            lns = para.text.split('\n')
            lines += lns
        
#         for line in lines:
#             print(line)
        report = self.parse_text(lines)
        return report
    
    def parse_text(self, text):
        
        report = Report()
        return report

class ReportPHQ:
    
    questions = [
        "Little Interest or pleasure in doing things", 
        "Feeling down depressed or hopeless", 
        "Trouble falling or staying asleep, or sleeping too much", 
        "Feeling tired or little energy", 
        "Poor appetite or overeating", 
        "Feeling bad about yourself - or that you are a failure or have let yourself or your family down", 
        "Trouble concentrating on things, such as reading the newspaper or watching television", 
        "Moving or speaking so slowly that other people could have noticed? Or the opposite - being so fidgety or restless that you have been moving around a lot more than usual", 
        "Thoughts that you would be better off dead or of hurting yourself in some way"
    ]
    
    def __init__(self, fileName='', text_lines=None):
        
#         self.fileNameFullPath = os.path.abspath(fileName)
        self.fileNameFullPath = os.path.abspath(fileName)
        self.fileName = os.path.basename(fileName)
        
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", self.fileName)
        if m:
            (m, d, y) = m[0]
#             (d, m, y) = m[0]
            self.reportDate = date(int(y), int(m), int(d))
        else:
            self.reportDate = None
            
        pos = self.fileName.find('_')
        if pos>=0:
            self.patient_id = self.fileName[:pos]
        else:
            self.patient_id = ''
            
        if not text_lines:
            if fileName=='':
                text_lines = []
            elif fileName.lower().endswith('pdf'):
                text_lines = self.read_pdf_report(self.fileNameFullPath)
            elif fileName.lower().endswith('docx'):
                text_lines = Report.read_report_file_docx(self.fileNameFullPath)
            elif fileName.lower().endswith('txt'):
                text_lines = Report.read_report_file_txt(self.fileNameFullPath)
        
        self.text = '\n'.join(text_lines)
        
        self.answers = ['']*len(ReportPHQ.questions)
        self.score = ''
        self.difficulty = ''
        
        self.parse_text(text_lines)
    
    def read_pdf_report(self, filename):
        basename = os.path.basename(filename)
        docname = "./data/fake_PHQ_reports/"+basename[:-3] + 'docx'
        textlines = Report.read_report_file_docx(docname)
        return textlines
    
    def read_pdf_report_ocr(self, filename):
        tmpfile = "./data/tmp.txt"
        os.remove(tmpfile)
        
        cmd = "./ocr/pdftotext -layout " + filename + " " + tmpfile
        subprocess.Popen(cmd)
        
        maxwait = 5
        waited = 0
        while not os.path.exists(tmpfile):
            sleep(1)
            waited += 1
        
        if not os.path.exists(tmpfile):
            return []
        
        textlines = Report.read_report_file_txt(tmpfile)
        
        lines = [line[:86] for line in textlines]
        return lines
            
        
    def setFileNameFullPath(self, fname):
        self.fileNameFullPath = os.path.abspath(fname)
        self.fileName = os.path.basename(self.fileNameFullPath)
        
        pos = self.fileName.find('_')
        if pos>=0:
            self.patient_id = self.fileName[:pos]
        else:
            self.patient_id = ''
            
    def getPatientID(self):
        return self.patient_id
    
    def getDate(self):
        return self.reportDate
    
    def getText(self):
        return self.text
    
    def getFileNameFullPath(self):
        return self.fileNameFullPath
    
    def getFileName(self):
        return self.fileName
    
    def getDifficulty(self):
        return self.difficulty
    
    def getPHQ9Ans(self):
        return self.answers
    
    def getScore(self):
        return self.score
    
    def getScoreValue(self):
        if self.score.isdigit():
            return int(self.score)
        else:
            return 0
    
    def getPHQAnswerValues(self):
        vals = [int(v) if v.isdigit() else 0 for v in self.answers]
        return vals
    
    def scoreAboveThreshold(self, threshold):
        if self.getScoreValue()>=threshold:
            return True
        else:
            return False
    
    def parse_text(self, lines):
        totalnum = len(lines)
        if totalnum==0:
            return
        
        if not self.reportDate:
            fulltext = ' '.join(lines)
            matches = re.findall("([0-1]?[0-9])[\.\/\-]([0-3]?[0-9])[\.\/\-]((19|20)[0-9]{2})", fulltext) # 03/12/2012, 03-12-2012, 03.12.2012  
            if matches:
                (m, d, y, yy) = matches[0]
                self.reportDate = date(int(y), int(m), int(d))
            
        last_section_line_num = 0
        #######################################################
        ##: Find answers
        lineNum = 0
        questions = ReportPHQ.questions
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            if line!='' and line[0].isdigit():
                break
            lineNum += 1

        while lineNum < totalnum:
            line = lines[lineNum].strip()
            if line.lower().startswith("phq-9 score:"):
                break
            
            lineNum += 1
            
            if len(line)<=2:
                continue
            
            if not line[0].isdigit():
                continue
            
            ans = line[0]
            for i, question in enumerate(questions):
                if difflib.SequenceMatcher(None, line[2:], question).ratio() > 0.7:
                    self.answers[i] = ans
                
        if lineNum < totalnum:
            lineScore  = lines[lineNum].strip()
            m = re.findall('\[(\d+)\]', lineScore)
            if m:
                self.score = m[0]
        
        
        #######################################################
        ##: Find difficulty answer
        q = "If you checked off any problems, how difficult"
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            s = line[:len(q)]
            if difflib.SequenceMatcher(None, s, q).ratio() > 0.8: ## e.g., Q-SSRS summag: 
                break
            lineNum += 1
            
        anses = ["Not difficult at all", "Somewhat difficult", "Very difficult", "Extremely difficult"]
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            
            line = line.replace('*', '')
            for ans in anses:
                endstr = line[-len(ans):]
                if difflib.SequenceMatcher(None, endstr, ans).ratio() > 0.9: ## e.g., Q-SSRS summag: 
                    self.difficulty = ans
                    break 
            if self.difficulty!='':
                break
        
    def display(self):
        for attr, value in self.__dict__.items():
            if attr=='text':
                continue
            
            if isinstance(value, str):
                print(attr + ": "+ value)
            if isinstance(value, list):
                print(attr + ": ")
                print(value)

    @staticmethod
    def generate_fake_reports(datapath):
        
        numpatients = 10
        numreports = [3 + random.randint(5, 10) for i in range(numpatients)]
        patientids = ['patient#' + str(i+1).zfill(2) for i in range(numpatients)]
        totalnum = int(math.fsum(numreports))
        numquestions = 9
        reports = []
        count = 0
        for i, ptid in enumerate(patientids):
            for j in range(numreports[i]):
                dd = str(random.randint(1, 28)).zfill(2)
                mm = str(random.randint(1, 12)).zfill(2)
                yy = str(random.randint(1990, 2017))
                filename = datapath + ptid + '_' + mm +'_' + dd+ '_' + yy + ".docx"
                
                level = random.randint(0, 3)
                scores = [level + random.randint(0, 2)-1 for i in range(numquestions)]
                finalscores = []
                for v in scores:
                    if v>3:
                        v = 3
                    elif v < 0:
                        v = 0
                    finalscores.append(v)
                
                report = ReportPHQ.generate_fake_report(filename, finalscores)
                reports.append(report)
                print(count)
                count += 1
        
        return reports
        
    @staticmethod
    def generate_fake_report(docxname, scores):
        
        document = Document(('./data/template PHQ-9.docx'))
        questions =["0 Little Interest", "0 Feeling down", "0 Trouble falling", "0 Feeling tired", 
                    "0 Poor appetite", "0 Feeling bad", "0 Trouble concentrating", 
                    "0 Moving or speaking", "0 Thoughts that"]
        difficultyAns = ["Not difficult at all", "Somewhat difficult", 
                         "**Very difficult**", "**Extremely difficult**"]

        overallscore = sum(scores)
        if overallscore>20:
            difficulty = difficultyAns[3]
        elif overallscore>13:
            difficulty = difficultyAns[2]
        elif overallscore>5:
            difficulty = difficultyAns[1]
        else:
            difficulty = difficultyAns[0]
            
            
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", docxname)
        if m:
            (d, m, y) = m[0]
            dt = d + '/' + m + '/' + y
        else:
            dt = ''
            
        n = 0
        for para in document.paragraphs:
#             if questions[0] not in para.text: 
#                 continue
            
            inline = para.runs
            for i in range(len(inline)):
                if '00' in inline[i].text:
                    inline[i].text = inline[i].text.replace("00", str(overallscore))
                elif '0' in inline[i].text and n<len(questions)+1:
                    if n==0:
                        n += 1
                        continue
                    text = inline[i].text.replace("0", str(scores[n-1]))
                    n += 1
                    inline[i].text = text
                elif 'DIFFICULTY' in inline[i].text:
                    inline[i].text = inline[i].text.replace("DIFFICULTY", difficulty)
                elif 'SERVICEDATE' in inline[i].text:
                    inline[i].text = inline[i].text.replace("SERVICEDATE", dt)

        document.save(docxname)
        
            
                        
class Report:
        
    C_SSRS_questions = [   
            "Have you wished you were dead or wished you could go to sleep and not wake up?",
            "Have you had any actual thoughts of killing yourself?",
            "Have you been thinking about how you might do this?",
            "Have you had these thoughts and had some intention of acting on them?",
            "Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan?",
            "Have you ever done anything, started to do anything, or prepared to do anything to end your life?",
            "Within the last three months?"
        ]
    def __init__(self, fileName='', text_lines=None, ocr=False):
        
#         self.fileNameFullPath = os.path.abspath(fileName)
        self.fileNameFullPath = fileName
        self.fileName = os.path.basename(fileName)
        
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", self.fileName)
        if m:
            (m, d, y) = m[0]
#             (d, m, y) = m[0]
            self.reportDate = date(int(y), int(m), int(d))
        else:
            self.reportDate = None
            
        pos = self.fileName.find('_')
        if pos>=0:
            self.patient_id = self.fileName[:pos]
        else:
            self.patient_id = ''
        
        if not text_lines:
            if fileName=='':
                text_lines = []
            elif fileName.lower().endswith('pdf'):
                text_lines = Report.read_report_file_pdf(self.fileNameFullPath)
            elif fileName.lower().endswith('docx'):
                text_lines = Report.read_report_file_docx(self.fileNameFullPath)
            elif fileName.lower().endswith('txt'):
                text_lines = Report.read_report_file_txt(self.fileNameFullPath)
        
        self.text = '\n'.join(text_lines)
        
        self.Patient_was_seen_for_30_minute_IBHC_appointment = ''
        self.IBHC_visit_number = 0
        
        self.GMH = ''
        self.Well = ''
        self.Psy = ''
        self.LFS = ''
        self.Anx = ''
        self.Dep = ''
        self.ETOH = ''
        self.Harm = ''
        self.SI = ''
        self.default_BHM_value = -1
        
        self.BHM20_Global_Mental_Health_Scale_Score = ''
        self.BHM20_Well_Score = ''
        self.BHM20_Psychological_Symptoms_Score = ''
        self.BHM20_Life_Functioning_Score = ''
        self.C_SSRS_Summary = ''
        
        self.C_SSRS_Ans = []

        self.parse_text(text_lines)
        
    
    def setFileNameFullPath(self, fname):
        self.fileNameFullPath = os.path.abspath(fname)
        self.fileName = os.path.basename(self.fileNameFullPath)
        
        pos = self.fileName.find('_')
        if pos>=0:
            self.patient_id = self.fileName[:pos]
        else:
            self.patient_id = ''
            
    
    @staticmethod
    def read_report_file_pdf(filename):
        ##: Determine if OCR needed
        with open(filename, 'rb') as f:
            pdf = PdfFileReader(f)
            page = pdf.getPage(0)
            text = page.extractText().strip()
            
            if len(text)<200: ##: Not many text, OCR is needed.
                text_lines = Utility.pdf2text(filename)
            else:
                text_lines = Utility.pdf2text_no_OCR(filename)
                
        return text_lines
            
    
#     @staticmethod
#     def read_report_file_pdf_no_OCR(filename):
#         text_lines = Utility.pdf2text_no_OCR(filename)
#         return text_lines
    
    @staticmethod
    def read_report_file_docx(filename):
#         if os.path.exists(filename):
#             i =1
        doc = docx.Document(filename)
#         doc = open(filename)
        lines = []
        for para in doc.paragraphs:
            t = Utility.clean_string_unicode(para.text)
            text = str(t)
            lns =text.split('\n')
            lines += lns
        return lines
    
    @staticmethod
    def read_report_file_txt(filename):
        try:
            with open(filename, 'rb') as f:
                lines = f.read().splitlines()
        except Exception as e:
            print(str(e))
            sys.exit()
        
        lines = [ln.strip() for ln in lines]
        lns = [Utility.clean_string_unicode(ln) for ln in lines]
        return lns
    
    def isBHM20Complete(self):
        if self.GMH=='' or self.Well=='' or self.Psy=='' or self.LFS=='' or self.Anx=='':
            return False
        
        if self.Dep=='' or self.ETOH=='' or self.Harm=='' or self.SI=='': 
            return False
        
        return True
    
    def isCSSRSComplete(self):
        if self.C_SSRS_Ans[1].startswith("Y"):
            for i in range(2,7):
                if self.C_SSRS_Ans[i]=='':
                    return False
        return True
        
    def getPatientID(self):
        return self.patient_id
    
    def getDate(self):
        return self.reportDate
    
    def getText(self):
        return self.text
    
    def getFileNameFullPath(self):
        return self.fileNameFullPath
    
    def getFileName(self):
        return self.fileName
    
    def getBHMScores(self):
        scores = [self.BHM20_Global_Mental_Health_Scale_Score, self.BHM20_Well_Score, 
                  self.BHM20_Psychological_Symptoms_Score, self.BHM20_Life_Functioning_Score]
        return scores
    
    def setBHMs(self, filename, scores):
        self.fileName = filename
        [self.GMH, self.Well, self.Psy, self.LFS, self.Anx, self.Dep, self.ETOH, self.Harm, self.SI] = scores
        
    def getBHMs(self):
        rec = [self.GMH, self.Well, self.Psy, self.LFS, self.Anx, self.Dep, self.ETOH, self.Harm, self.SI]
        return rec
    
    def getBHMValues(self):
        rec = [self.GMH, self.Well, self.Psy, self.LFS, self.Anx, self.Dep, self.ETOH, self.Harm, self.SI]
        vals = [Utility.s2float(s) for s in rec]
        vals = [v if v!=None else self.default_BHM_value for v in vals]
        return vals
    
    def getAppointment(self):
        return self.Patient_was_seen_for_30_minute_IBHC_appointment
    
    def getVisitNumber(self):
        return self.IBHC_visit_number
    
    def getBHM20Scores(self):
        rec = [self.BHM20_Global_Mental_Health_Scale_Score, self.BHM20_Well_Score, 
               self.BHM20_Psychological_Symptoms_Score, self.BHM20_Life_Functioning_Score]
        return rec
    
    def getCSSRSummary(self):
        return self.C_SSRS_Summary
    
    def getCSSRAnswers(self):
        return self.C_SSRS_Ans
    
    def parse_text(self, lines):
        totalnum = len(lines)
        if totalnum==0:
            return
        
        if not self.reportDate:
            fulltext = ' '.join(lines)
            matches = re.findall("([0-1]?[0-9])[\.\/\-]([0-3]?[0-9])[\.\/\-]((19|20)[0-9]{2})", fulltext) # 03/12/2012, 03-12-2012, 03.12.2012  
            if matches:
                (m, d, y, yy) = matches[0]
                self.reportDate = date(int(y), int(m), int(d))
            
        last_section_line_num = 0
        #######################################################
        ##: Find answer for "Patient was seen for 30 minute IBHC appointment"
        lineNum = 0
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("Patient was seen for 30 minute IBHC appointment"):
                parts = line.split(':')
                if len(parts)>1:
                    self.Patient_was_seen_for_30_minute_IBHC_appointment = parts[1].strip()
                last_section_line_num = lineNum
                break 
            lineNum += 1
            
        if lineNum >= totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num   
            
        #######################################################
        ##: Find answer for "IBHC visit number"
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("IBHC visit number"):
                parts = line.split(':')
                if len(parts)>1:
                    self.IBHC_visit_number = parts[1].strip()
                last_section_line_num = lineNum
                break 
            lineNum += 1
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
            
        #######################################################
        ##: Find BHM-20 measures
        BHM20s = ['GMH','Well','Psy','LFS', 'Anx', 'Dep','ETOH','Harm', 'SI']
        
        regex_BHM20 = {'GMH':"GMH *- (\d*.?\d*) *Well", 
                    'Well':"Well *- (\d*.?\d*) *Psy", 
                    'Psy':"Psy *- (\d*.?\d*) *LFS", 
                    'LFS':"LFS *- (\d*.?\d*) *Anx", 
                    'Anx':"Anx *- (\d*.?\d*) *Dep", 
                    'Dep':"Dep *- (\d*.?\d*) *ETOH", 
                    'ETOH':"ETOH *- (\d*.?\d*) *Harm", 
                    'Harm':"Harm *- (\d*.?\d*) *", 
                    'SI':"SI *- ?(\d*.?\d*) *"
                    }
        
        line_scores = ''
        for i in range(lineNum, totalnum):
            line = lines[i]
            found = [s for s in BHM20s if s in line]
            if len(found) > 2: ##: More than 2 scores are found in this line
                line_scores = line.strip()
                lineNum = i
                break
        
        if line_scores!='':
            while lineNum < totalnum:
                lineNum += 1
                line = lines[lineNum].strip()  ##: strip to ignore empty line
#                 if re.match('[a-zA-Z]', line):
                if line.startswith('BHM'):
                    break
                else:
                    line_scores += line 
            
            for var, regex in regex_BHM20.items():
                m = re.findall(regex, line_scores, re.I)
                if m:
                    v = m[0].strip()
                    setattr(self, var, v)
            last_section_line_num = lineNum
        else:            
            lineNum = last_section_line_num  ##: Not found. Point reset to the last section
                
            
        #######################################################
        ##: Find BHM-20 scores
        BHM20_scores = {
        "BHM20_Global_Mental_Health_Scale_Score": "Global Mental Health Scale Score",
        "BHM20_Well_Score": "Well- Score",
        "BHM20_Psychological_Symptoms_Score": "Psychological Symptoms Score",
        "BHM20_Life_Functioning_Score": "Life Functioning Score"
        }
        
        var_BHM20_scores = [
            "BHM20_Global_Mental_Health_Scale_Score",
            "BHM20_Well_Score",
            "BHM20_Psychological_Symptoms_Score",
            "BHM20_Life_Functioning_Score"
        ]
        
        ifound = 0
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            
            if line=='':
                continue
            
            if line.startswith('BHM-20 '):
                parts = re.split('[:;]', line)
                if len(parts)>1:
                    scr = parts[1]
                    scr = scr.replace('*','').strip()
                    var = var_BHM20_scores[ifound]
                    setattr(self, var, scr)
                    
                    last_section_line_num = lineNum
                    
                    ifound += 1
                    if ifound >= len(var_BHM20_scores):
                        break
            else:
                break
        
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
            
        #######################################################
        ##: Find C-SSRS answers
        lineNumCSSRS = -1
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.upper().startswith('C-SSRS CDS'):
                lineNumCSSRS = lineNum + 1
                break 
            elif line.lower().startswith('in the past month'):
                lineNumCSSRS = lineNum
                break
            else:
                lineNum += 1
        
        lineNum = lineNumCSSRS
        CSSRHeader = ['1:', '2:', '3:', '4:', '5:','6a', '6b']
        self.C_SSRS_Ans = ['']*len(CSSRHeader)
        
        ifound = 0
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            
            if len(line)<=2:
                continue
            
            if line[:2] in CSSRHeader:
                if lines[lineNum][:2] not in CSSRHeader and not lines[lineNum].startswith("In your"):
                    line += ' ' + lines[lineNum]
                    lineNum += 1
                index = CSSRHeader.index(line[:2])
                parts = line.split('?')
                if len(parts)>1 and parts[-1]!='':
                    ans = parts[-1].split()[0]  ##: only the first token, handle e.g., "No (skip to ...)"
                    ans = ans.replace('*','').strip()  ##: ...: **Yes**
                    self.C_SSRS_Ans[index] = ans
                
                    last_section_line_num = lineNum
                    
                if index==len(CSSRHeader)-1: ## Found the last one
                    break
                
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
        
                
        #######################################################
        ##: Find C-SSRS Summary
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            if difflib.SequenceMatcher(None, line, 'C-SSRS Summary:').ratio() > 0.7: ## e.g., Q-SSRS summag: 
#             if line.startswith('C-SSRS Summary'):
#             if line.startswith('c-ssrs summary'):
                break 
            
        CSSRS_summary = ''
        while lineNum < totalnum: ##: not the end
            line = lines[lineNum].strip()
            lineNum += 1
            if line == '':
                continue
            elif line.startswith('IBHC'):
                break
            CSSRS_summary += line +'\n'
            lineNum += 1
            
        self.C_SSRS_Summary = CSSRS_summary
        
    def parse_text_v1(self, lines):
        totalnum = len(lines)
        if totalnum==0:
            return
        
        last_section_line_num = 0
        #######################################################
        ##: Find answer for "Patient was seen for 30 minute IBHC appointment"
        lineNum = 0
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("Patient was seen for 30 minute IBHC appointment"):
                parts = line.split(':')
                if len(parts)>1:
                    self.Patient_was_seen_for_30_minute_IBHC_appointment = parts[1].strip()
                last_section_line_num = lineNum
                break 
            lineNum += 1
            
        if lineNum >= totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num   
            
        #######################################################
        ##: Find answer for "IBHC visit number"
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("IBHC visit number"):
                parts = line.split(':')
                if len(parts)>1:
                    self.IBHC_visit_number = parts[1].strip()
                last_section_line_num = lineNum
                break 
            lineNum += 1
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
            
        #######################################################
        ##: Find BHM-20 measures
        BHM20s = ['GMH','Well','Psy','LFS', 'Anx', 'Dep','ETOH','Harm', 'SI']
        
        regex_BHM20 = {'GMH':"GMH *- (\d*.?\d*) *Well", 
                    'Well':"Well *- (\d*.?\d*) *Psy", 
                    'Psy':"Psy *- (\d*.?\d*) *LFS", 
                    'LFS':"LFS *- (\d*.?\d*) *Anx", 
                    'Anx':"Anx *- (\d*.?\d*) *Dep", 
                    'Dep':"Dep *- (\d*.?\d*) *ETOH", 
                    'ETOH':"ETOH *- (\d*.?\d*) *Harm", 
                    'Harm':"Harm *- (\d*.?\d*) *", 
                    'SI':"SI *- ?(\d*.?\d*) *"
                    }
        
        line_scores = ''
        for i in range(lineNum, totalnum):
            line = lines[i]
            found = [s for s in BHM20s if s in line]
            if len(found) > 2: ##: More than 2 scores are found in this line
                line_scores = line.strip()
                lineNum = i
                break
        
        if line_scores!='':
            while lineNum < totalnum:
                lineNum += 1
                line = lines[lineNum].strip()  ##: strip to ignore empty line
                if re.match('[a-zA-Z]', line):
                    break
                else:
                    line_scores += line 
            
            for var, regex in regex_BHM20.items():
                m = re.findall(regex, line_scores, re.I)
                if m:
                    setattr(self, var, m[0])
            last_section_line_num = lineNum
        else:            
            lineNum = last_section_line_num  ##: Not found. Point reset to the last section
                
            
        #######################################################
        ##: Find BHM-20 scores
        BHM20_scores = {
        "BHM20_Global_Mental_Health_Scale_Score": "Global Mental Health Scale Score",
        "BHM20_Well_Score": "Well- Score",
        "BHM20_Psychological_Symptoms_Score": "Psychological Symptoms Score",
        "BHM20_Life_Functioning_Score": "Life Functioning Score"
        }
        
        var_BHM20_scores = [
            "BHM20_Global_Mental_Health_Scale_Score",
            "BHM20_Well_Score",
            "BHM20_Psychological_Symptoms_Score",
            "BHM20_Life_Functioning_Score"
        ]
        
        ifound = 0
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            
            if line=='':
                continue
            
            if line.startswith('BHM-20 '):
                parts = re.split('[:;]', line)
                if len(parts)>1:
                    scr = parts[1]
                    scr = scr.replace('*','').strip()
                    var = var_BHM20_scores[ifound]
                    setattr(self, var, scr)
                    
                    last_section_line_num = lineNum
                    
                    ifound += 1
                    if ifound >= len(var_BHM20_scores):
                        break
            else:
                break
        
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
            
        #######################################################
        ##: Find C-SSRS answers
        lineNumCSSRS = -1
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.upper().startswith('C-SSRS CDS'):
                lineNumCSSRS = lineNum + 1
                break 
            elif line.lower().startswith('in the past month'):
                lineNumCSSRS = lineNum
                break
            else:
                lineNum += 1
        
        lineNum = lineNumCSSRS
        CSSRHeader = ['1:', '2:', '3:', '4:', '5:','6a', '6b']
        self.C_SSRS_Ans = ['']*len(CSSRHeader)
        
        ifound = 0
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            
            if len(line)<=2:
                continue
            
            if line[:2] in CSSRHeader:
                index = CSSRHeader.index(line[:2])
                parts = line.split('?')
                if len(parts)>1:
                    ans = parts[-1].split()[0]  ##: only the first token, handle e.g., "No (skip to ...)"
                    ans = ans.replace('*','').strip()  ##: ...: **Yes**
                    self.C_SSRS_Ans[index] = ans
                
                    last_section_line_num = lineNum
                    
                if index==len(CSSRHeader)-1: ## Found the last one
                    break
                
        if lineNum>=totalnum: ##: Not found. Point reset to the last section
            lineNum = last_section_line_num  
        
                
        #######################################################
        ##: Find C-SSRS Summary
        while lineNum < totalnum:
            line = lines[lineNum].strip()
            lineNum += 1
            if difflib.SequenceMatcher(None, line, 'C-SSRS Summary:').ratio() > 0.7: ## e.g., Q-SSRS summag: 
#             if line.startswith('C-SSRS Summary'):
#             if line.startswith('c-ssrs summary'):
                break 
            
        CSSRS_summary = ''
        while lineNum < totalnum: ##: not the end
            line = lines[lineNum].strip()
            lineNum += 1
            if line == '':
                continue
            elif line.startswith('IBHC'):
                break
            CSSRS_summary += line +'\n'
            lineNum += 1
            
        self.C_SSRS_Summary = CSSRS_summary
        
    def parse_text_docx(self, lines):
        totalnum = len(lines)
        if totalnum==0:
            return
        
        #######################################################
        ##: Find answer for "Patient was seen for 30 minute IBHC appointment"
        lineNum = 0
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("Patient was seen for 30 minute IBHC appointment"):
                parts = line.split(':')
                if len(parts)>1:
                    self.Patient_was_seen_for_30_minute_IBHC_appointment = parts[1].strip()
                break 
            lineNum += 1
        if lineNum>=totalnum: ##: Not found. Point reset to the beginning
            lineNum = 0    
            
        #######################################################
        ##: Find answer for "IBHC visit number"
        while lineNum < totalnum:
            line = lines[lineNum]
            if line.startswith("IBHC visit number"):
                parts = line.split(':')
                if len(parts)>1:
                    self.IBHC_visit_number = parts[1].strip()
                break 
            lineNum += 1
        if lineNum>=totalnum: ##: Not found. Point reset to the beginning
            lineNum = 0  
            

        #######################################################
        ##: Find BHM-20 measures
        regex_BHM20 = {'GMH':"GMH - (\d*.?\d*) ?Well", 
                    'Well':"Well - (\d*.?\d*) ?Psy", 
                    'Psy':"Psy - (\d*.?\d*) ?LFS", 
                    'LFS':"LFS - (\d*.?\d*) ?Anx", 
                    'Anx':"Anx - (\d*.?\d*) ?Dep", 
                    'Dep':"Dep - (\d*.?\d*) ?ETOH", 
                    'ETOH':"ETOH - (\d*.?\d*) ?Harm", 
                    'Harm':"Harm - (\d*.?\d*) ?SI", 
                    'SI':"SI - (\d*.?\d*) ?$"
                    }
        
        line_scores = ''
        for lineNum, line in enumerate(lines):
            if 'GMH' in line and 'Well' in line:
                line_scores = line
                break
        
        for var, regex in regex_BHM20.items():
            m = re.findall(regex, line)
            if m:
                setattr(self, var, m[0])
                
        #######################################################
        ##: Find BHM-20 scores
        BHM20_scores = {
        "BHM20_Global_Mental_Health_Scale_Score": "Global Mental Health Scale Score",
        "BHM20_Well_Score": "Well- Score",
        "BHM20_Psychological_Symptoms_Score": "Psychological Symptoms Score",
        "BHM20_Life_Functioning_Score": "Life Functioning Score"
        }
        
        var_BHM20_scores = [
            "BHM20_Global_Mental_Health_Scale_Score",
            "BHM20_Well_Score",
            "BHM20_Psychological_Symptoms_Score",
            "BHM20_Life_Functioning_Score"
        ]
        
        totalnum = len(lines)
        while lineNum < totalnum:
            if lines[lineNum].startswith('BHM-20 ') and lines[lineNum+1].startswith('BHM-20 '):
                break 
            lineNum += 1
        
        if lineNum < totalnum: ##: not the end
            for i, var in enumerate(var_BHM20_scores):
                parts = lines[lineNum+i].split(':')
                if len(parts)>1:
                    scr = parts[1]
                    scr = scr.replace('*','').strip()
                    setattr(self, var, scr)
        
        #######################################################
        ##: Find C-SSRS answers
        lineNumCSSRS = -1
        while lineNum < totalnum:
            if lines[lineNum].startswith('C-SSRS CDS'):
                lineNumCSSRS = lineNum + 1
                break 
            lineNum += 1
        
        while lineNumCSSRS < totalnum:
            if lines[lineNumCSSRS].startswith('1: ') and lines[lineNumCSSRS+1].startswith('2: '):
                break 
            lineNumCSSRS += 1
        
        CSSRHeader = ['1:', '2:', '3:', '4:', '5:','6a:', '6b']
        self.C_SSRS_Ans = ['']*len(CSSRHeader)
        if lineNumCSSRS < totalnum: ##: not the end
            for i, header in enumerate(CSSRHeader):
                if not lines[lineNumCSSRS].startswith(header):  ##: Handle the line before 6a.
                    lineNumCSSRS += 1   
                    
                if lines[lineNumCSSRS].startswith(header):
                    parts = lines[lineNumCSSRS].split('?')
                    if len(parts)>1:
                        ans = parts[-1].split()[0]  ##: handle e.g., "No (skip to ...)"
                        self.C_SSRS_Ans[i] = ans

                lineNumCSSRS += 1   
                
        #######################################################
        ##: Find C-SSRS Summary
        while lineNumCSSRS < totalnum:
            if lines[lineNumCSSRS].startswith('C-SSRS Summary'):
                lineNumCSSRS += 1
                break 
            lineNumCSSRS += 1
        CSSRS_summary = ''
        while lineNumCSSRS < totalnum: ##: not the end
            line = lines[lineNumCSSRS]
            if line.strip()=='':
                break
            CSSRS_summary += line
            lineNumCSSRS += 1
            
        self.C_SSRS_Summary = CSSRS_summary
        
    def display(self):
        for attr, value in self.__dict__.items():
            if attr=='text':
                continue
            
            if isinstance(value, str):
                print(attr + ": "+ value)
            if isinstance(value, list):
                print(attr + ": ")
                print(value)
    
    
    @staticmethod
    def generate_fake_reports(datapath, pdf=True):
        
        numpatients = 10
        numreports = [3 + random.randint(5, 10) for i in range(numpatients)]
        patientids = ['patient#' + str(i+1).zfill(2) for i in range(numpatients)]
        totalnum = int(math.fsum(numreports))
        
        BHM20Names = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        data = []
        for scr in BHM20Names:
            mn = random.random()*3 + 1
            vals = [random.gauss(mn, 0.5) for i in range(totalnum)]
            vs = []
            for v in vals:
                if v<1:
                    nv = 1
                elif v>4:
                    nv = 4
                else:
                    nv = v
                vs.append(nv)
            data.append(vs)
        
        reports = []
        count = 0
        for i, ptid in enumerate(patientids):
            for j in range(numreports[i]):
                dd = str(random.randint(1, 28)).zfill(2)
                mm = str(random.randint(1, 12)).zfill(2)
                yy = str(random.randint(1990, 2017))
                BHMs = ["{:4.2f}".format(data[k][count]) for k in range(len(BHM20Names))]
                if random.random()>0.7:
                    BHMs[random.randint(0,8)] = ''
                    
                if pdf:
                    filename = datapath + ptid + '_' + mm +'_' + dd+ '_' + yy + ".pdf"
                    report = Report.generate_fake_report_pdf(filename, BHMs)
                else:
                    filename = datapath + ptid + '_' + mm +'_' + dd+ '_' + yy + ".docx"
                    if random.random()>0.7:
                        report = Report.generate_fake_incomplete_report(filename, BHMs)
                    else:
                        report = Report.generate_fake_report(filename, BHMs)
                reports.append(report)
                print(count)
                count += 1
        
        return reports
        
    @staticmethod
    def generate_fake_report(docxname, BHM20Values):
        
        document = Document()
        
        ##########################
        ##: Create paragraph 1
        ##########################
        para1 = """        * Final Report *
Admin
Patient referred by: Other: NP Julagay 
Source of information: Self 
Feedback given to PCM: Y 
Patient seen by trainee: N 
Patient was seen for 30 minute IBHC appointment: {} 
Pathways: _ 
IBHC visit number: {}
Patient given IBHC brochure: Y """.format(['Y','N'][random.randint(0,1)], random.randint(1,3))
        document.add_paragraph(para1)
        
        ##########################
        ##: Create paragraph 2
        ##########################
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", docxname)
        if m:
            (d, m, y) = m[0]
            dt = d + '/' + m + '/' + y
        else:
            dt = ''
        GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI = BHM20Values
#         para2 = """Chief Complaint: anxiety
# 
# Behavioral Health Measure-20: (Only for IBHC provider use as per contract)
# {} GMH - {:4.2f} Well - {:4.2f} Psy - {:4.2f} LFS - {:4.2f} Anx - {:4.2f} Dep - {:4.2f} ETOH - {:4.2f} Harm - {:4.2f} SI - {:4.2f}
# """.format(dt, GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI)
        para2 = """Chief Complaint: anxiety

Behavioral Health Measure-20: (Only for IBHC provider use as per contract)
{} GMH - {} Well - {} Psy - {} LFS - {} Anx - {} Dep - {} ETOH - {} Harm - {} SI - {}
""".format(dt, GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI)
        document.add_paragraph(para2)
        
        ##########################
        ##: Create paragraph 3
        ##########################
        strBHM20s = ["Normal", "Mild Distress", "**Moderate Distress**", "**Severe Distress**"]
        para3 = """BHM-20 Global Mental Health Scale Score: {}
BHM-20 Well- Score: {}
BHM-20 Psychological Symptoms Score: {} 
BHM-20 Life Functioning Score: {}""".format(strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)], 
           strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)])
        
        document.add_paragraph(para3)
        
        ##########################
        ##: Create paragraph 4
        ##########################
        ans=['Yes', 'No']
        para4 = """C-SSRS CDS
In the past month have you?
1: Have you wished you were dead or wished you could go to sleep and not wake up? {}
2: Have you had any actual thoughts of killing yourself? {} (skip to question #6 if No)
3: Have you been thinking about how you might do this? {}
4: Have you had these thoughts and had some intention of acting on them? {}
5: Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan? No
In your lifetime have you?
6a: Have you ever done anything, started to do anything, or prepared to do anything to end your life? {} 
6b: Within the last three months? {}

C-SSRS Summary:
Patient was screened for suicide risk using the C-SSRS.No suicide risk items were endorsed. _
Actions taken: no actions needed""".format(ans[random.randint(0, 1)], ans[0], ans[random.randint(0, 1)], 
                                           ans[random.randint(0, 1)], ans[random.randint(0, 1)], ans[random.randint(0, 1)], ans[random.randint(0, 1)])
        
        document.add_paragraph(para4)
        
        ##########################
        ##: Create paragraph 4
        ##########################
        para5 = """IBHC Review of Systems
Systemic: feeling tired (fatigue).
Gastrointestinal: increase in appetite .
Neurological: No disorientation. 
Psychological: hypersomnia . Normal enjoyment of activities and impulsive behavior 

IBHC Mental Status Exam
The patient reported: No decrease in concentrating ability. 
Psychological: Not thinking about suicide , not having a suicide plan , and no stated intent to commit suicide . No homicidal thoughts , not thinking of a way to do it , and without a stated intent to kill. 
General Appearance: Alert . Well developed . Well nourished . In no acute distress .
Neurological: No disorientation was observed - oriented to place, person, time, and situation . No hallucinations . Memory was unimpaired . Remote memory was not impaired . Recent memory was not impaired . Judgement was not impaired .
Speech: Rate was normal. Rate was not slowed. Not pressured . Tone was not monotone . Volume was normal . No articulation abnormalities. No language abnormalities were demonstrated .
Demonstrated Behavior: No decreased eye-to-eye contact was observed .
Attitude: Cooperative .
Mood: Anxious 
Affect: Normal. Not labile. Not flat. Not constricted. Showed no irritability . Congruent with mood. 
Thought Processes: Not impaired - they were linear, logical, and goal directed. Evaluation of connectedness showed no deficiency . Attention demonstrated no abnormalities. 
Thought Content: Insight was intact . No delusions. 

IBHC anxiety history of present illness CDS 
Description of Symptoms: waking up feeling scared, racing heart rate, shortness of breath, numbness in hands, muscle tension, more emotional, some nausea, panic attacks, self-critical, excessive worry 
Duration of Problem:over a year, general anxiety since 18 yr 
Factors correlated with onset:originally when she moved out on her own, recently relationship issue 
Frequency of symptoms:daily (every morning) 
Severity of symptoms:worst 10 of 10 (lately 8 of 10), 5 of 10, now 6 of 10 
Psychosocial factors: recently single (ended relationship 2 months ago). Support - local friends, her dad (hesitant in reaching out to anyone). Enjoys - laughing, her dog, being outside, hiking, dirt biking. To relax - open windows, watch TV (binge watch)
Aggravating factors: talking, feeling vulnerable 
Alleviating factors: going for a walk, listen to music, set future goals 
Current tx:none 
Past tx:none 
Functional impact:personal wellbeing, relationships.
IBHC anxiety intervention and patient education
Anxiety Intervention: 
[xx] Trained in relaxation strategies 
[_] Trained in improving communication skills 
[xx] Discussed potential treatments for anxiety (i.e. PE, CPT, CBT) 
[xx] Discussed various factors related to the development and maintenance of anxiety
(including biological, cognitive, behavioral, and environmental factors). 
[_] Developed Crisis Response plan. 
[xx] Trained in strategies for increasing balanced thinking. 
[_] Other:
Assessment and Plan
Anxiety
Dr Chen, this Pt was referred to IBHC by NP Julagay for anxiety issues. Pt reported that she has struggled with anxiety symptoms for the past 8 years, but has notice an increase in severity and frequency over the past year. Pt's reported symptoms appear to be consistent with a generalized anxiety disorder. Pt stated she is open to engaging in cognitive behavioral strategies, but is resistant to medication treatment. Pt and IBHC discussed and agreed upon the following plan.

Anxiety Recommendations for patient:
1) Pt was provided with verbal and written information on anxiety and symptoms management strategies.
2) Pt elected to begin with daily relaxation breathing exercises as discussed and demonstrated (5 minutes, 1-2 times per day) - Breath2Relax phone app for additional info/guidance
3) Pt to f/u with IBHC in 2 weeks (consider introducing worry management strategies)

Anxiety Recommendations for PCM Team:
During next scheduled PCM appointment, inquire about use and effectiveness of cognitive behavioral strategies in managing her anxiety symptoms. No additional recommendations at this time.
        """
        document.add_paragraph(para5)
        document.save(docxname)
        
    @staticmethod
    def generate_fake_incomplete_report(docxname, BHM20Values):
        
        document = Document()
        
        ##########################
        ##: Create paragraph 1
        ##########################
        para1 = """        * Final Report *
Admin
Patient referred by: Other: NP Julagay 
Source of information: Self 
Feedback given to PCM: Y 
Patient seen by trainee: N 
Patient was seen for 30 minute IBHC appointment: {} 
Pathways: _ 
IBHC visit number: {}
Patient given IBHC brochure: Y """.format(['Y','N'][random.randint(0,1)], random.randint(1,3))
        document.add_paragraph(para1)
        
        ##########################
        ##: Create paragraph 2
        ##########################
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", docxname)
        if m:
            (d, m, y) = m[0]
            dt = d + '/' + m + '/' + y
        else:
            dt = ''
        GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI = BHM20Values
#         para2 = """Chief Complaint: anxiety
# 
# Behavioral Health Measure-20: (Only for IBHC provider use as per contract)
# {} GMH - {:4.2f} Well - {:4.2f} Psy - {:4.2f} LFS - {:4.2f} Anx - {:4.2f} Dep - {:4.2f} ETOH - {:4.2f} Harm - {:4.2f} SI - {:4.2f}
# """.format(dt, GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI)
        para2 = """Chief Complaint: anxiety

Behavioral Health Measure-20: (Only for IBHC provider use as per contract)
{} GMH - {} Well - {} Psy - {} LFS - {} Anx - {} Dep - {} ETOH - {} Harm - {} SI - {}
""".format(dt, GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI)
        document.add_paragraph(para2)
        
        ##########################
        ##: Create paragraph 3
        ##########################
        strBHM20s = ["Normal", "Mild Distress", "**Moderate Distress**", "**Severe Distress**"]
        para3 = """BHM-20 Global Mental Health Scale Score: {}
BHM-20 Well- Score: {}
BHM-20 Psychological Symptoms Score: {} 
BHM-20 Life Functioning Score: {}""".format(strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)], 
           strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)])
        
        document.add_paragraph(para3)
        
        ##########################
        ##: Create paragraph 4
        ##########################
        ans=['Yes', 'No']
        a = []
        for i in range(3):
            a.append(ans[random.randint(0,1)])
        
        a[random.randint(0,2)]=''
        
        
        para4 = """C-SSRS CDS
In the past month have you?
1: Have you wished you were dead or wished you could go to sleep and not wake up? {}
2: Have you had any actual thoughts of killing yourself? {} (skip to question #6 if No)
3: Have you been thinking about how you might do this? {}
4: Have you had these thoughts and had some intention of acting on them? {}
5: Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan? No
In your lifetime have you?
6a: Have you ever done anything, started to do anything, or prepared to do anything to end your life? {} 
6b: Within the last three months? {}

C-SSRS Summary:
Patient was screened for suicide risk using the C-SSRS.No suicide risk items were endorsed. _
Actions taken: no actions needed""".format(ans[random.randint(0, 1)], ans[0], a[0], a[1], a[2], ans[random.randint(0, 1)], ans[random.randint(0, 1)])
        
        document.add_paragraph(para4)
        
        ##########################
        ##: Create paragraph 4
        ##########################
        para5 = """IBHC Review of Systems
Systemic: feeling tired (fatigue).
Gastrointestinal: increase in appetite .
Neurological: No disorientation. 
Psychological: hypersomnia . Normal enjoyment of activities and impulsive behavior 

IBHC Mental Status Exam
The patient reported: No decrease in concentrating ability. 
Psychological: Not thinking about suicide , not having a suicide plan , and no stated intent to commit suicide . No homicidal thoughts , not thinking of a way to do it , and without a stated intent to kill. 
General Appearance: Alert . Well developed . Well nourished . In no acute distress .
Neurological: No disorientation was observed - oriented to place, person, time, and situation . No hallucinations . Memory was unimpaired . Remote memory was not impaired . Recent memory was not impaired . Judgement was not impaired .
Speech: Rate was normal. Rate was not slowed. Not pressured . Tone was not monotone . Volume was normal . No articulation abnormalities. No language abnormalities were demonstrated .
Demonstrated Behavior: No decreased eye-to-eye contact was observed .
Attitude: Cooperative .
Mood: Anxious 
Affect: Normal. Not labile. Not flat. Not constricted. Showed no irritability . Congruent with mood. 
Thought Processes: Not impaired - they were linear, logical, and goal directed. Evaluation of connectedness showed no deficiency . Attention demonstrated no abnormalities. 
Thought Content: Insight was intact . No delusions. 

IBHC anxiety history of present illness CDS 
Description of Symptoms: waking up feeling scared, racing heart rate, shortness of breath, numbness in hands, muscle tension, more emotional, some nausea, panic attacks, self-critical, excessive worry 
Duration of Problem:over a year, general anxiety since 18 yr 
Factors correlated with onset:originally when she moved out on her own, recently relationship issue 
Frequency of symptoms:daily (every morning) 
Severity of symptoms:worst 10 of 10 (lately 8 of 10), 5 of 10, now 6 of 10 
Psychosocial factors: recently single (ended relationship 2 months ago). Support - local friends, her dad (hesitant in reaching out to anyone). Enjoys - laughing, her dog, being outside, hiking, dirt biking. To relax - open windows, watch TV (binge watch)
Aggravating factors: talking, feeling vulnerable 
Alleviating factors: going for a walk, listen to music, set future goals 
Current tx:none 
Past tx:none 
Functional impact:personal wellbeing, relationships.
IBHC anxiety intervention and patient education
Anxiety Intervention: 
[xx] Trained in relaxation strategies 
[_] Trained in improving communication skills 
[xx] Discussed potential treatments for anxiety (i.e. PE, CPT, CBT) 
[xx] Discussed various factors related to the development and maintenance of anxiety
(including biological, cognitive, behavioral, and environmental factors). 
[_] Developed Crisis Response plan. 
[xx] Trained in strategies for increasing balanced thinking. 
[_] Other:
Assessment and Plan
Anxiety
Dr Chen, this Pt was referred to IBHC by NP Julagay for anxiety issues. Pt reported that she has struggled with anxiety symptoms for the past 8 years, but has notice an increase in severity and frequency over the past year. Pt's reported symptoms appear to be consistent with a generalized anxiety disorder. Pt stated she is open to engaging in cognitive behavioral strategies, but is resistant to medication treatment. Pt and IBHC discussed and agreed upon the following plan.

Anxiety Recommendations for patient:
1) Pt was provided with verbal and written information on anxiety and symptoms management strategies.
2) Pt elected to begin with daily relaxation breathing exercises as discussed and demonstrated (5 minutes, 1-2 times per day) - Breath2Relax phone app for additional info/guidance
3) Pt to f/u with IBHC in 2 weeks (consider introducing worry management strategies)

Anxiety Recommendations for PCM Team:
During next scheduled PCM appointment, inquire about use and effectiveness of cognitive behavioral strategies in managing her anxiety symptoms. No additional recommendations at this time.
        """
        document.add_paragraph(para5)
        document.save(docxname)
        
    @staticmethod
    def generate_fake_report_pdf(docxname, BHM20Values):
        """Create attendance report in PDF format."""
        styles=getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Text', alignment=TA_JUSTIFY, fontSize=11))
        styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY, fontSize=9, leading=10))
        styles.add(ParagraphStyle(name='TimePeriod', alignment=TA_CENTER))
        styles.add(ParagraphStyle(name='ReportTitle', alignment=TA_CENTER, fontSize=15))
        styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER, fontSize=12, fontName='Times-Italic'))
 
        Story = []               
        ##########################
        ##: Create paragraph 1
        ##########################
        para1 = """        * Final Report *<br /><br />
Admin<br />
Patient referred by: Other: NP Julagay <br />
Source of information: Self <br />
Feedback given to PCM: Y <br />
Patient seen by trainee: N <br />
Patient was seen for 30 minute IBHC appointment: {} <br />
Pathways: _ <br />
IBHC visit number: {}<br />
Patient given IBHC brochure: Y <br /><br />""".format(['Y','N'][random.randint(0,1)], random.randint(1,3))
        Story.append(Paragraph(para1, styles["Text"]))
        
        ##########################
        ##: Create paragraph 2
        ##########################
        m = re.findall("(\d\d)_(\d\d)_(\d\d\d\d)", docxname)
        if m:
            (d, m, y) = m[0]
            dt = d + '/' + m + '/' + y
        else:
            dt = ''
        GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI = BHM20Values
        para2 = """Chief Complaint: anxiety<br /><br />

Behavioral Health Measure-20: (Only for IBHC provider use as per contract)<br />
{} GMH - {:4.2f} Well - {:4.2f} Psy - {:4.2f} LFS - {:4.2f} Anx - {:4.2f} Dep - {:4.2f} ETOH - {:4.2f} Harm - {:4.2f} SI - {:4.2f}
""".format(dt, GMH,Well,Psy,LFS, Anx, Dep,ETOH,Harm, SI)

        Story.append(Paragraph(para2, styles["Text"]))  
              
        ##########################
        ##: Create paragraph 3
        ##########################
        strBHM20s = ["Normal", "Mild Distress", "**Moderate Distress**", "**Severe Distress**"]
        para3 = """<br />BHM-20 Global Mental Health Scale Score: {}<br />
BHM-20 Well- Score: {}<br />
BHM-20 Psychological Symptoms Score: {} <br />
BHM-20 Life Functioning Score: {}<br />""".format(strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)], 
           strBHM20s[random.randint(0, 3)], strBHM20s[random.randint(0, 3)])

        Story.append(Paragraph(para3, styles["Text"]))      
          
        ##########################
        ##: Create paragraph 4
        ##########################
        ans=['Yes', 'No']
        para4 = """<br />C-SSRS CDS<br />
In the past month have you?<br/>
1: Have you wished you were dead or wished you could go to sleep and not wake up? {}<br />
2: Have you had any actual thoughts of killing yourself? {} (skip to question #6 if No)<br />
3: Have you been thinking about how you might do this? {}<br />
4: Have you had these thoughts and had some intention of acting on them? {}<br />
5: Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan? No<br />
In your lifetime have you?<br />
6a: Have you ever done anything, started to do anything, or prepared to do anything to end your life? {} <br />
6b: Within the last three months? {}<br /><br />

C-SSRS Summary:<br />
Patient was screened for suicide risk using the C-SSRS.No suicide risk items were endorsed. _<br />
Actions taken: no actions needed<br />""".format(ans[random.randint(0, 1)], ans[0], ans[random.randint(0, 1)], 
                                           ans[random.randint(0, 1)], ans[random.randint(0, 1)], ans[random.randint(0, 1)], ans[random.randint(0, 1)])
        
        Story.append(Paragraph(para4, styles["Text"]))
        
        ##########################
        ##: Create paragraph 4
        ##########################
        para5 = """<br />IBHC Review of Systems<br />
Systemic: feeling tired (fatigue).<br />
Gastrointestinal: increase in appetite .<br />
Neurological: No disorientation. <br />
Psychological: hypersomnia . Normal enjoyment of activities and impulsive behavior <br /><br />

IBHC Mental Status Exam<br />
The patient reported: No decrease in concentrating ability. <br />
Psychological: Not thinking about suicide , not having a suicide plan , and no stated intent to commit suicide . No homicidal thoughts , not thinking of a way to do it , and without a stated intent to kill. <br />
General Appearance: Alert . Well developed . Well nourished . In no acute distress .<br />
Neurological: No disorientation was observed - oriented to place, person, time, and situation . No hallucinations . Memory was unimpaired . Remote memory was not impaired . Recent memory was not impaired . Judgement was not impaired .<br />
Speech: Rate was normal. Rate was not slowed. Not pressured . Tone was not monotone . Volume was normal . No articulation abnormalities. No language abnormalities were demonstrated .<br />
Demonstrated Behavior: No decreased eye-to-eye contact was observed .<br />
Attitude: Cooperative .<br />
Mood: Anxious <br />
Affect: Normal. Not labile. Not flat. Not constricted. Showed no irritability . Congruent with mood. <br />
Thought Processes: Not impaired - they were linear, logical, and goal directed. Evaluation of connectedness showed no deficiency . Attention demonstrated no abnormalities. <br />
Thought Content: Insight was intact . No delusions. <br /><br />

IBHC anxiety history of present illness CDS <br />
Description of Symptoms: waking up feeling scared, racing heart rate, shortness of breath, numbness in hands, muscle tension, more emotional, some nausea, panic attacks, self-critical, excessive worry <br />
Duration of Problem:over a year, general anxiety since 18 yr <br />
Factors correlated with onset:originally when she moved out on her own, recently relationship issue <br />
Frequency of symptoms:daily (every morning) <br />
Severity of symptoms:worst 10 of 10 (lately 8 of 10), 5 of 10, now 6 of 10 <br />
Psychosocial factors: recently single (ended relationship 2 months ago). Support - local friends, her dad (hesitant in reaching out to anyone). Enjoys - laughing, her dog, being outside, hiking, dirt biking. To relax - open windows, watch TV (binge watch)<br />
Aggravating factors: talking, feeling vulnerable <br />
Alleviating factors: going for a walk, listen to music, set future goals <br />
Current tx:none <br />
Past tx:none <br />
Functional impact:personal wellbeing, relationships.<br />
IBHC anxiety intervention and patient education<br />
Anxiety Intervention: <br />
[xx] Trained in relaxation strategies <br />
[_] Trained in improving communication skills <br />
[xx] Discussed potential treatments for anxiety (i.e. PE, CPT, CBT) <br />
[xx] Discussed various factors related to the development and maintenance of anxiety<br />
(including biological, cognitive, behavioral, and environmental factors). <br />
[_] Developed Crisis Response plan. <br />
[xx] Trained in strategies for increasing balanced thinking. <br />
[_] Other:<br />
Assessment and Plan<br />
Anxiety<br />
Dr Chen, this Pt was referred to IBHC by NP Julagay for anxiety issues. Pt reported that she has struggled with anxiety symptoms for the past 8 years, but has notice an increase in severity and frequency over the past year. Pt's reported symptoms appear to be consistent with a generalized anxiety disorder. Pt stated she is open to engaging in cognitive behavioral strategies, but is resistant to medication treatment. Pt and IBHC discussed and agreed upon the following plan.<br />
<br />
Anxiety Recommendations for patient:<br />
1) Pt was provided with verbal and written information on anxiety and symptoms management strategies.<br />
2) Pt elected to begin with daily relaxation breathing exercises as discussed and demonstrated (5 minutes, 1-2 times per day) - Breath2Relax phone app for additional info/guidance<br />
3) Pt to f/u with IBHC in 2 weeks (consider introducing worry management strategies)<br />
<br />
Anxiety Recommendations for PCM Team:<br />
During next scheduled PCM appointment, inquire about use and effectiveness of cognitive behavioral strategies in managing her anxiety symptoms. No additional recommendations at this time.<br />
        """

        Story.append(Paragraph(para5, styles["Text"]))       
         
        doc = SimpleDocTemplate(docxname, pagesize=letter, rightMargin=72, leftMargin=72, topMargin=36, bottomMargin=36)
        try:
            doc.build(Story) 
        except Exception as e:
            print(str(e))
            return
        
#         
#         report = Report()
#         BHM20Names = ['GMH','Well','Psy','LFS', 'Anx', 'Dep','ETOH','Harm', 'SI']
#         for i, attr in enumerate(BHM20Names):
#             setattr(report, attr, BHM20Values[i])
#             

    
if __name__ == '__main__':
#     fn = "C:/Engility/HERMES/T2IBHC1.docx"
# #     os.startfile(fn)
#     subprocess.Popen(["start", fn], shell=True)
    
#     vals = [random.random() for i in range(9)]
#     Report.generate_fake_reports('./data/fake_reports/', pdf=False)
    
#     fname = "./data/patient#02_02_12_2012.pdf"
#     fname = "./data/fake_reports/patient#01_11_10_2004.docx"
#     report = Report(fname)
#     report.isBHM20Complete()
#     Report.generate_fake_report_pdf(fname, vals)
#     os.startfile(os.path.abspath(fname))
    
#     reportParser = ReportParser()
#     report = reportParser.parse_file('./data/patient#1.docx')
#     s = Utility.clean_string_unicode('s0\xe2\x80\x94sfasd')
# #     s = Utility.clean_string_unicode(u"sfadsafsd")
#     print(s)
#     report = Report('./data/patient#1.docx')
#     report = Report('./data/patient#2.docx')
#     report = Report('./data/T2IBHC1.pdf')
#     report = Report('./data/T2IBHC1.txt')
#     ReportPHQ.generate_fake_report("./data/fakePHQ_03_21_2012.docx")
#     ReportPHQ.generate_fake_reports("./data/fake_PHQ_reports/")
#     report = Report('C:/Engility/HERMES/T2IBHC1.docx')
    report = ReportPHQ('C:/Projects/MentalHealthDataAnalyzer/data/fake_PHQ_PDFs/patient#01_01_25_2000.pdf')
    report.display()
#     text = Utility.pdf2text('./data/T2IBHC1.pdf')
#     print(text)
    
    print('Program finished!!!')
    