#!/usr/bin python
# -*- coding: utf-8 -*-
'''
Created on Jul 23, 2018

@author: Wei.Wang@Engility.com
'''

# from PyQt5.QtWidgets import *
# from PyQt5.QtGui import *
# from PyQt5.QtCore import *
import os, sys, math, statistics, logging, xlwt, random
# , sys, csv, logging, gc
# import sqlite3, re, os, nltk 
from time import sleep 
from ReportParser import Report, ReportParser, ReportPHQ

# from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
# import matplotlib.mlab as mlab
from matplotlib import rcParams
from __builtin__ import enumerate
from PyQt4.Qt import QGridLayout
from docutils.nodes import option
rcParams.update({'figure.autolayout': True})

# from PyQt4.QtWidgets import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *
# from PyQt4 import QtGui

# from PySide.QtCore import *
# from PySide.QtGui import *

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_pdf import PdfPages

import matplotlib
# matplotlib.rcParams['backend.qt4']='PySide'

# from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
# from matplotlib.figure import Figure

# class Utility:
#     @staticmethod
#     def get_html_complements(htmlfile):
#         fdir, basename = os.path.split(htmlfile)
#         pos = basename.rfind('.')
#         filename = basename[:pos]
#         
#         complement_dir = fdir + '/' + filename + '_files'
#         if not os.path.exists(complement_dir):
#             return
#         
#         files = os.listdir(complement_dir)
#         jpgs = [f for f in files if f[-3:]=='jpg']
#         return jpgs

class MouseToggleTreeWidget(QTreeWidget):
    focusLost = pyqtSignal()
     
    def __init__(self, parent=None):
        super(MouseToggleTreeWidget, self).__init__(parent)
        self.mouse_enabled = True
    
    def mousePressEvent(self, event):
        logging.debug('captured mouse press event')
        if self.mouse_enabled == True:
            super(MouseToggleTreeWidget, self).mousePressEvent(event)
    
    def mouseReleaseEvent(self, event):
        logging.debug('captured mouse release event')
        if self.mouse_enabled == True:
            super(MouseToggleTreeWidget, self).mouseReleaseEvent(event)
        
    def mouseMoveEvent(self, event):
        logging.debug('captured mouse move event')
        if self.mouse_enabled == True:
            super(MouseToggleTreeWidget, self).mouseMoveEvent(event)
            
    def focusOutEvent(self,event):
        #self.emit("lostFocus")
        #self.emit(Signal("lostFocus")
        self.focusLost.emit()
        
        
class QuestionRadio(QFrame):
    def __init__(self, question, parent = None):
        super(QuestionRadio, self).__init__(parent)
        self.setLayout(QHBoxLayout())
        self.radioYes = QRadioButton("Yes") 
        self.radioNo = QRadioButton("No") 
        radiogroup = QButtonGroup()
        radiogroup.addButton(self.radioYes)
        radiogroup.addButton(self.radioNo)
        self.layout().addWidget(QLabel(question))
        self.layout().addWidget(self.radioYes)
        self.layout().addWidget(self.radioNo)
        self.layout().addWidget(QLabel())
        self.layout().setStretch(3,100)
        
class RadioDuo(QFrame):
    def __init__(self, parent = None):
        super(RadioDuo, self).__init__(parent)
        self.setLayout(QHBoxLayout())
        self.radioYes = QRadioButton("Yes") 
        self.radioNo = QRadioButton("No") 
        self.radiogroup = QButtonGroup()
        self.radiogroup.addButton(self.radioYes)
        self.radiogroup.addButton(self.radioNo)
        self.layout().addWidget(self.radioYes)
        self.layout().addWidget(self.radioNo)
        self.layout().setContentsMargins(0,0,0,0)
    
    def setYes(self):
        self.radioYes.setChecked(True)
    
    def setNo(self):
        self.radioNo.setChecked(True)
    
    def clear(self):
        checked = self.radiogroup.checkedButton()
        self.radiogroup.setExclusive(False)
        if checked:
            checked.setChecked(False)
        self.radiogroup.setExclusive(True)
        
class QuestionCombox(QFrame):
    def __init__(self, question, choices, parent = None):
        super(QuestionCombox, self).__init__(parent)
        self.setLayout(QHBoxLayout())
        self.cbx = QComboBox() 
        self.cbx.addItems(choices)
        self.layout().addWidget(QLabel(question))
        self.layout().addWidget(self.cbx)
        self.layout().addWidget(QLabel())
        self.layout().setStretch(2,100)
        
class ReportFrame(QFrame):
    def __init__(self, parent = None):
        super(ReportFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = self.mainFrame.reports
        
        self.setLayout(QVBoxLayout())
         
        #############################################
        ##: Test information section
        
        grpTop = QGroupBox('Admin')
        grpTop.setLayout(QFormLayout())   
        self.qAppointment = RadioDuo()    
        self.qAppointment.setFixedWidth(100)
        self.spinVisitNumber =QSpinBox()
        self.spinVisitNumber.setFixedWidth(50) 
        grpTop.layout().addRow(QLabel('Patient was seen for 30 minute IBHC appointment:'), self.qAppointment) 
        grpTop.layout().addRow(QLabel('IBHC visit number:'), self.spinVisitNumber) 
        
        grpTest = QGroupBox('Behavioral Health Measure-20')
#         grpTest.setLayout(QVBoxLayout())
        frmBHMs = QFrame()
        frmBHMs.setLayout(QGridLayout())
        BHMNames = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        grpTest.setLayout(QGridLayout())   
        self.txtBHMs = []
        for i, name in enumerate(BHMNames):
            txt = QLineEdit()
            self.txtBHMs.append(txt)
            frmBHMs.layout().addWidget(QLabel(name), 0, 2*i) 
            frmBHMs.layout().addWidget(txt, 0, 2*i+1) 
        
        frmBHMScores = QFrame()
        frmBHMScores.setLayout(QFormLayout()) 
        self.strBHM20s = ["", "Normal", "Mild Distress", "Moderate Distress", "Severe Distress"]
        
        self.BHM20Scores = ["BHM-20 Global Mental Health Scale Score", "BHM-20 Well- Score", 
                       "BHM-20 Psychological Symptoms Score", "BHM-20 Life Functioning Score"]
        self.cbxBHM20Scores = []
        for score in self.BHM20Scores:
            cbx = QComboBox()
            cbx.addItems(self.strBHM20s)
            cbx.setFixedWidth(150)
            self.cbxBHM20Scores.append(cbx)
            frmBHMScores.layout().addRow(QLabel(score), cbx)
        
        grpTest.layout().addWidget(frmBHMs)
        grpTest.layout().addWidget(frmBHMScores)
        
        C_SSRS_questions = [   
            "In the past month have you?",
            "1. Have you wished you were dead or wished you could go to sleep and not wake up?",
            "2. Have you had any actual thoughts of killing yourself?",
            "3. Have you been thinking about how you might do this?",
            "4. Have you had these thoughts and had some intention of acting on them?",
            "5. Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan?",
            "In your lifetime have you?",
            "6a. Have you ever done anything, started to do anything, or prepared to do anything to end your life?",
            "6b. Within the last three months?"
        ]
        
        self.radioCSSRS = []
        grpCSSRS = QGroupBox('C-SSRS CDS')
        grpCSSRS.setLayout(QVBoxLayout())
        
        frmCSSRS = QFrame()
        frmCSSRS.setLayout(QFormLayout())   
        for q in C_SSRS_questions:
            if q.endswith("have you?"):
                frmCSSRS.layout().addRow(q, None)
                continue
            rad = RadioDuo()
            rad.setFixedWidth(100)
            self.radioCSSRS.append(rad)
            frmCSSRS.layout().addRow('  '+q, rad)
        self.txtCSSRSummary = QTextEdit()
        grpCSSRS.layout().addWidget(frmCSSRS)
        grpCSSRS.layout().addWidget(QLabel("C-SSRS Summary:"))
        grpCSSRS.layout().addWidget(self.txtCSSRSummary)
        
        
        self.layout().addWidget(grpTop)
        self.layout().addWidget(grpTest)
        self.layout().addWidget(grpCSSRS)
        self.layout().addWidget(QLabel())
        self.layout().setStretch(3,10)

    def set_current_report(self, report):
        self.clearReport()
        
        appointment = report.getAppointment()
        if appointment.upper().startswith('Y'):
            self.qAppointment.setYes()
        else:
            self.qAppointment.setNo()
        
        visitnum = report.getVisitNumber()
        try:
            num = int(visitnum)
            self.spinVisitNumber.setValue(num)
        except:
            pass
        
        BHMs = report.getBHMs()
        for i, txt in enumerate(self.txtBHMs):
            txt.setText(BHMs[i])
        
        scores = report.getBHM20Scores()
        for i, cbx in enumerate(self.cbxBHM20Scores):
            if scores[i] in self.strBHM20s:
                index = self.strBHM20s.index(scores[i])
            else:
                index = 0
            cbx.setCurrentIndex(index)
        
        CSSRAns = report.getCSSRAnswers()
        for i, rd in enumerate(self.radioCSSRS):
            if CSSRAns[i].upper().startswith('Y'):
                rd.setYes()
            elif CSSRAns[i].upper().startswith('N'):
                rd.setNo()
            
        summary = report.getCSSRSummary()
        self.txtCSSRSummary.setText(summary)
    
    def clearReport(self):
        self.qAppointment.clear()
        self.spinVisitNumber.setValue(0)
        for txt in self.txtBHMs:
            txt.setText('')
        
        for cbx in self.cbxBHM20Scores:
            cbx.setCurrentIndex(0)
        
        for rd in self.radioCSSRS:
            rd.clear()
            
        self.txtCSSRSummary.setText('')
        

class ReportPHQ9Frame(QFrame):
    def __init__(self, parent = None):
        super(ReportPHQ9Frame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = self.mainFrame.reports
        
        self.setLayout(QVBoxLayout())
         
        #############################################
        ##: Test information section


        questionsPHQ = [
            "Little Interest or pleasure in doing things", 
            "Feeling down depressed or hopeless", 
            "Trouble falling or staying asleep, or sleeping too much", 
            "Feeling tired or little energy", 
            "Poor appetite or overeating", 
            "Feeling bad about yourself - or that you are a failure or have let yourself or your family down", 
            "Trouble concentrating on things, such as reading the newspaper or watching television", 
            "Moving or speaking so slowly that other people could have noticed? Or the opposite - being so fidgety or restless that you have been moving around a lot more than usual", 
            "Thoughts that you would be better off dead or of hurting yourself in some way"
        ]
        note = "(0-not at all, 1-several days, 2-more than half the days, 3-nearly every day)"
        grpTop = QGroupBox('DEPRESSION SCREENING / MONITORING (PHQ-9)')
        grpTop.setLayout(QGridLayout())   
#         (0-not at all, 1-several days, 2-more than half the days, 3-nearly every day)
        
        self.spinBoxQuestions = []
        for i, q in enumerate(questionsPHQ):
            spinBox = QSpinBox()
            spinBox.setFixedWidth(50)
            self.spinBoxQuestions.append(spinBox)
            grpTop.layout().addWidget(spinBox, i, 0, 1, 1)
            grpTop.layout().addWidget(QLabel(q), i, 1, 1, 1)
        grpTop.layout().addWidget(QLabel(note), i+1, 0, 1, 2)
        
        frmScore = QFrame()
        frmScore.setLayout(QHBoxLayout())
        self.spinboxScore = QSpinBox()
        frmScore.layout().addWidget(QLabel('PHQ-9 score: '))
        frmScore.layout().addWidget(self.spinboxScore)
        frmScore.layout().addWidget(QLabel(''))
        frmScore.layout().setStretch(2, 10)
        
        frmDifficulty = QFrame()
        frmDifficulty.setLayout(QVBoxLayout())
        self.cboxDifficulty = QComboBox()
        q = "If you checked off any problems, how difficult have these problems made it for you to do your work, take care of things at home, or get along with other people? "
        lb = QLabel(q)
        lb.setWordWrap(True)
        frmDifficulty.layout().addWidget(lb)
        frmDifficulty.layout().addWidget(self.cboxDifficulty)
#         frmDifficulty.layout().addWidget(QLabel(''))
#         frmDifficulty.layout().setStretch(2, 10)
        
        self.difficultyAns = ["", "Not difficult at all", "Somewhat difficult", "Very difficult", "Extremely difficult"]
        self.cboxDifficulty.addItems(self.difficultyAns)
        self.cboxDifficulty.setFixedWidth(160)
            
        self.layout().addWidget(grpTop)
        self.layout().addWidget(frmScore)
        self.layout().addWidget(frmDifficulty)
        self.layout().addWidget(QLabel())
        self.layout().setStretch(3,10)

    def set_current_report(self, report):
        self.clearReport()
        
        answers = report.getPHQ9Ans()
        for i, spinbox in enumerate(self.spinBoxQuestions):
            ans = answers[i]
            if ans.isdigit():
                spinbox.setValue(int(ans))
        
        score = report.getScore()
        if score.isdigit():
            self.spinboxScore.setValue(int(score))
        
        difficulty = report.getDifficulty()
        if difficulty in self.difficultyAns:
            index = self.difficultyAns.index(difficulty)
            self.cboxDifficulty.setCurrentIndex(index)
        
    def clearReport(self):
        for spinbox in self.spinBoxQuestions:
            spinbox.setValue(0)
        
        self.spinboxScore.setValue(0)
        self.cboxDifficulty.setCurrentIndex(0)



class PatientPHQScoreTableFrame(QGroupBox):
    def __init__(self, reports = None, parent = None):
        super(PatientPHQScoreTableFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
        
        self.setTitle('Historical Data')
        self.setLayout(QHBoxLayout())
        

        self.tableData = QTableWidget()
        
        ##: Table initialization                
        
        self.header = ['Date', 'Score'] + ['Q'+str(i+1) for i in range(9)] + ['Difficulty']
        self.tableData.setColumnCount(len(self.header))
        self.tableData.setHorizontalHeaderLabels(self.header)
        self.tableData.verticalHeader().hide()
        palette = self.tableData.horizontalHeader().setStyleSheet("QHeaderView::section {background-color:lightblue}")##: Populate data into the table
#         self.populateTableData(self.reports)
#         self.tableData.resizeColumnsToContents()
#         self.tableData.resizeRowsToContents()        
        
        questions = ReportPHQ.questions
        qs = ['Q'+str(i+1)+": "+q for i, q in enumerate(questions)]      
        note = "(0-not at all, 1-several days, 2-more than half the days, 3-nearly every day)"
        s = "\n".join(qs + [note])  
        labelQ = QLabel(s)
        labelQ.setWordWrap(True)
        self.layout().addWidget(self.tableData)
        self.layout().addWidget(labelQ)
        
    
    def populateTableData(self, reports):
        self.tableData.clear()        
        self.tableData.setHorizontalHeaderLabels(self.header)
        self.tableData.setRowCount(len(reports))
        
        if len(reports)==0:
            return
        
        ##: Populate table data
        for i, report in enumerate(reports):
            row = report.getPHQ9Ans()
            dt = report.getDate().isoformat()
            score = report.getScore()
            difficulty = report.getDifficulty()
            self.populateTableRow([dt, score] + row + [difficulty], i)
        
#         ##: Calculate statistics
#         ncol = len(self.header)-1
#         data = []
#         for i, report in enumerate(reports):
#             row = report.getBHMValues()
#             data.append(row)
        
        self.tableData.resizeColumnsToContents()
            
    def populateTableRow(self, record, irow):
        """Populate the record to the table row"""
        for icol in range(len(record)):
            text = record[icol]
            if not isinstance(text, str):
                text = "%4.2f"%(text)
            ##: set the text columns
            item = QTableWidgetItem(text)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                
            self.tableData.setItem(irow, icol, item)


class PatientPHQPlotFrame(QGroupBox):
    def __init__(self, reports, parent = None):
        super(PatientPHQPlotFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QHBoxLayout())
        self.setTitle('PHQ-9 Score')
        
        self.setAutoFillBackground(True)


#         frmThreshold = QFrame()
#         frmThreshold.setLayout(QHBoxLayout())
#         self.chkThreshold = QCheckBox('Show Threshold:')
#         self.chkThreshold.stateChanged.connect(self.on_chk_threshold_changed)
#         self.txtThreshold = QLineEdit('20')
#         self.txtThreshold.setFixedWidth(30)
#         self.txtThreshold.setDisabled(True)
#         self.txtThreshold.textChanged.connect(self.plot)
#         frmThreshold.layout().addWidget(QLabel(''))
#         frmThreshold.layout().addWidget(self.chkThreshold)
#         frmThreshold.layout().addWidget(self.txtThreshold)
#         frmThreshold.layout().setStretch(0,100)
        
        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        self.layout().addWidget(self.canvas)
#         self.layout().addWidget(frmThreshold)
#         self.layout().setStretch(0, 100)
        
        
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
    
#     def on_chk_threshold_changed(self):
#         if self.chkThreshold.isChecked():
#             self.txtThreshold.setEnabled(True)
#         else:
#             self.txtThreshold.setDisabled(True)
#         self.plot()
        
    def plot(self):
        if len(self.reports)==0:
            return
        
        data = [report.getScoreValue() for report in self.reports]
        
#         ax = self.figure.add_subplot(111)
        # the histogram of the data
        x = [report.getDate() for report in self.reports]
        

        self.ax.cla()
        self.ax.plot(x, data)
        self.ax.set_xlabel('Date')
        self.ax.set_ylabel('Score')
        self.ax.set_ylim([0, 28])

        self.ax.grid(True)
        
        sysOptions = self.mainFrame.get_sys_options()
        if 'PHQ-9' in sysOptions and sysOptions["PHQ-9"]!=None:
            threshold = sysOptions["PHQ-9"]

            markers_above = [(t, data[i]) for i, t in enumerate(x) if data[i]>=threshold]
            markers_x = [pt[0] for pt in markers_above]
            markers_y = [pt[1] for pt in markers_above]
            
            baseline_x = [x[0], x[-1]]
            baseline_y = [threshold]*2
            
            self.ax.plot(baseline_x, baseline_y, color = 'red')
            self.ax.text(x[0], threshold+1, "Threshold", color = 'red', fontsize=16)
            self.ax.plot(markers_x, markers_y, marker='x', color = 'red', linestyle = 'None')

        self.canvas.draw()
        
class FileExplorerFrame(QFrame):
    def __init__(self, reports, parent = None):
        super(FileExplorerFrame, self).__init__(parent)
        self.reports = reports
        self.mainFrame = parent
        
        self.setLayout(QVBoxLayout())
        
        self.frmReportsAll = ReportsAllFrame(reports, self.mainFrame)
        self.frmReportsPatients = ReportsPatientFrame(reports, self.mainFrame)
        self.stackedWidget = QStackedWidget()
        self.stackedWidget.addWidget(self.frmReportsAll)
        self.stackedWidget.addWidget(self.frmReportsPatients)
        self.stackedWidget.setCurrentIndex(0)
        
        self.frmRadioType = QFrame()
        self.frmRadioType.setLayout(QHBoxLayout())
        self.radioReport = QRadioButton("By Report") 
        self.radioPatient = QRadioButton("By Patient") 
        self.radioReport.clicked.connect(self.type_radio_clicked)
        self.radioPatient.clicked.connect(self.type_radio_clicked)
        self.radiogroup = QButtonGroup()
        self.radiogroup.addButton(self.radioReport)
        self.radiogroup.addButton(self.radioPatient)
        self.frmRadioType.layout().addWidget(self.radioReport)
        self.frmRadioType.layout().addWidget(self.radioPatient)
        
        grpFiles = QGroupBox('Reports')
        grpFiles.setLayout(QVBoxLayout())
        grpFiles.layout().addWidget(self.stackedWidget)
        grpFiles.layout().addWidget(self.frmRadioType)
        grpFiles.layout().setStretch(0, 100)
        
        self.stackedWidget.setCurrentIndex(0)
        self.radioReport.setChecked(True)
        
        self.layout().addWidget(grpFiles)
        
        
    def populate_reports(self, reports):
        self.frmReportsAll.populate_reports(reports)
        self.frmReportsPatients.populate_reports(reports)
    
    def add_report(self, report):
        self.frmReportsAll.add_report(report)
#         self.frmReportsPatients.add_report(report)
        
    def type_radio_clicked(self):
        if self.radioPatient.isChecked():
            self.stackedWidget.setCurrentIndex(1)
            self.mainFrame.reportTabs.addTab(self.mainFrame.frmPatient, 'History')
        elif self.radioReport.isChecked():
            self.stackedWidget.setCurrentIndex(0)
            if self.mainFrame.reportTabs.count()>2:
                self.mainFrame.reportTabs.removeTab(2)
    
    def clear_reports(self):
        self.frmReportsAll.fileExplorer.clear()
        self.frmReportsPatients.fileExplorer.clear() 

class ReportsAllFrame(QFrame):
    def __init__(self, reports, parent = None):
        super(ReportsAllFrame, self).__init__(parent)
        self.reports = reports
        self.mainFrame = parent
        
        self.setLayout(QVBoxLayout())
        
#         grpFiles = QGroupBox('Report List')
#         grpFiles.setLayout(QVBoxLayout())
# #         grpFiles.setTitle()

        self.fileExplorer = MouseToggleTreeWidget(self)
        
        self.fileExplorer.setSortingEnabled(True)
#         self.fileExplorer.setSelectionMode(QAbstractItemView.SingleSelection)
        self.fileExplorer.setSelectionMode(QAbstractItemView.ExtendedSelection)
#         self.fileExplorer.setSelectionBehavior(QAbstractItemView.SelectRows)
        header = ['All']
        self.fileExplorer.setColumnCount(len(header))
        self.fileExplorer.setHeaderLabels(header)    
        self.fileExplorer.itemSelectionChanged.connect(self.on_current_selection_changed)

        self.layout().addWidget(self.fileExplorer)
                
        self.populate_reports(reports)
        
    def add_report(self, report):
        fullpath = report.getFileNameFullPath().strip()
        fullpath = fullpath.replace(' ', '%20')
        filename = report.getFileName().strip()
        pos = filename.rfind('.')
        basename = filename[:pos]
        basename = basename.replace(' ', '%20')
        ext = filename[pos+1:]
#         labelText = '{0} (.<a href="file:///{1}">{2}</a>)'.format(basename, fullpath, ext)
        labelText = '{0} (.<a href="file:///{1}">{2}</a>)'.format(basename, fullpath, ext)
#         labelText = '{0} (.<a href="os.startfile({1})">{2}</a>)'.format(basename, fullpath, ext)
        labelText = labelText.replace('\\', '/')
#         labelText = labelText.replace(':', '\:')
        
        if self.mainFrame.report_type=="PHQ-9":
            sysOptions = self.mainFrame.get_sys_options()
            if 'PHQ-9' in sysOptions:
                threshold = sysOptions["PHQ-9"]
                if threshold!=None and report.scoreAboveThreshold(threshold):
                    labelText += '  <span><font color="red" size="3"><b>&#33;</b><font></span>'
           
        linkLabel = QLabel(labelText, self.fileExplorer)
#         linkLabel.setOpenExternalLinks(True)
        linkLabel.linkActivated.connect(lambda: self.link_clicked(fullpath))
        linkLabel.setToolTip(report.getFileNameFullPath())
        item = QTreeWidgetItem(self.fileExplorer, [''])
        self.fileExplorer.setItemWidget(item, 0, linkLabel)
    
    def link_clicked(self, fullpath):
        os.startfile(fullpath)
        
    def populate_reports(self, reports):
        self.fileExplorer.clear()
        
        self.reports = reports
#         nz = len(str(num))
#         for i, index in enumerate(self.mainWindow.selectedArticleIndices):
#             article = self.articles[index]                        
#             row = ['', str(i).zfill(nz)] + article.get_summary_row()
#             item = QTreeWidgetItem(self.article_table, row)
#             item.setTextAlignment(1, Qt.AlignCenter)
            
        for report in reports:
            self.add_report(report)    
    
    def on_current_selection_changed(self):
        selected = self.fileExplorer.selectedItems()
        if not selected:
            return
        
#         fulltextStart = 0
#         text = self.structuredFrame.textFrame.article_text.toPlainText()
#         fulltextEnd = len(text)
        item = selected[0]
        if item:
            index = self.fileExplorer.indexOfTopLevelItem(item)
            report = self.reports[index]
            self.mainFrame.set_current_report(report)
    
#     def export_reports(self):
#         selectedItems = self.fileExplorer.selectedItems()
#         if not selectedItems:
#             return
#         
#         selected_reports = []
#         for item in selectedItems:
#             index = self.fileExplorer.indexOfTopLevelItem(item)
#             report = self.reports[index]
#             selected_reports.append(report)
#         
#         self.mainFrame.mainWindow.export_xls(selected_reports)
# 
#     def delete_report(self):
#         return
    

class ReportsPatientFrame(QFrame):
    def __init__(self, reports, parent = None):
        super(ReportsPatientFrame, self).__init__(parent)
        self.reports = reports
        self.mainFrame = parent
        
        self.setLayout(QVBoxLayout())
        
        self.fileExplorer = MouseToggleTreeWidget(self)
        
#         self.fileExplorer.setSortingEnabled(True)
#         self.fileExplorer.setSelectionMode(QAbstractItemView.SingleSelection)
        self.fileExplorer.setSelectionMode(QAbstractItemView.ExtendedSelection)
#         self.fileExplorer.setSelectionBehavior(QAbstractItemView.SelectRows)
        header = ['Patients']
        self.fileExplorer.setColumnCount(len(header))
        self.fileExplorer.setHeaderLabels(header)    
        self.fileExplorer.itemSelectionChanged.connect(self.on_current_selection_changed) 
        self.fileExplorer.setColumnHidden(1, True)

#            
#         self.fileExplorer.setContextMenuPolicy(Qt.ActionsContextMenu)
#         action_delete_report = QAction("Delete", self.fileExplorer)
#         action_delete_report.triggered.connect(self.delete_report)
#         action_delete_report.setStatusTip("Delete selected annotation")
#         
#         action_export_report = QAction("Export", self.fileExplorer)
#         action_export_report.triggered.connect(self.export_reports)
#         action_export_report.setStatusTip("Delete selected annotation")
#         
#         self.fileExplorer.addAction(action_delete_report)
#         self.fileExplorer.addAction(action_export_report)
           
#         self.article_table.header().resizeSection(12,360)
        
#         self.fileExplorer.setColumnHidden(0, True)  
        self.layout().addWidget(self.fileExplorer)
        
        
        self.populate_reports(reports)
        
    def add_report(self, parent, report):
        fullpath = report.getFileNameFullPath().strip()
        fullpath = fullpath.replace(' ', '%20')
        filename = report.getFileName().strip()
        pos = filename.rfind('.')
        basename = filename[:pos]
        basename = basename.replace(' ', '%20')
        ext = filename[pos+1:]
#         labelText = '{0} (.<a href="file:///{1}">{2}</a>)'.format(basename, fullpath, ext)
        labelText = '{0} (.<a href="file:///{1}">{2}</a>)'.format(basename, fullpath, ext)
#         labelText = '{0} (.<a href="os.startfile({1})">{2}</a>)'.format(basename, fullpath, ext)
        labelText = labelText.replace('\\', '/')
        
        if self.mainFrame.report_type=="PHQ-9":
            sysOptions = self.mainFrame.get_sys_options()
            if 'PHQ-9' in sysOptions:
                threshold = sysOptions["PHQ-9"]
                if threshold!=None and report.scoreAboveThreshold(threshold):
                    labelText += '  <span><font color="red" size="3"><b>&#33;</b><font></span>'
                                    
#         labelText = labelText.replace(':', '\:')
        linkLabel = QLabel(labelText, self.fileExplorer)
#         linkLabel.setOpenExternalLinks(True)
        linkLabel.linkActivated.connect(lambda: self.link_clicked(fullpath))
        linkLabel.setToolTip(report.getFileNameFullPath())
        
        index = self.reports.index(report)
        item = QTreeWidgetItem(parent, ['', str(index)])
        self.fileExplorer.setItemWidget(item, 0, linkLabel)
    
    def link_clicked(self, fullpath):
        os.startfile(fullpath)
    
    def organize_reports(self, reports):
        patient_ids = []
        for report in reports:
            rptname = report.getFileName()
            pos = rptname.find('_')
            if pos < 0:
                continue
            ptid = rptname[:pos]
            if ptid not in patient_ids:
                patient_ids.append(ptid)
#         patient_ids.sort(reverse=True)
        patient_ids.sort()

        patient_reports = {}
        for ptid in patient_ids:
            rpts = [report for report in reports if report.getFileName().startswith(ptid)]
            rpts.sort(key = lambda r:r.getDate())
            patient_reports[ptid] = rpts
        
        return patient_ids, patient_reports
    
    def populate_reports(self, reports):
        self.fileExplorer.clear()
        
        self.reports = reports
#         nz = len(str(num))
#         for i, index in enumerate(self.mainWindow.selectedArticleIndices):
#             article = self.articles[index]                        
#             row = ['', str(i).zfill(nz)] + article.get_summary_row()
#             item = QTreeWidgetItem(self.article_table, row)
#             item.setTextAlignment(1, Qt.AlignCenter)
        
        self.patient_ids, self.patient_reports = self.organize_reports(reports)
        
        for ptid in self.patient_ids:
            item = QTreeWidgetItem(self.fileExplorer, [ptid,''])
            rpts = self.patient_reports[ptid]
            for rpt in rpts:
                self.add_report(item, rpt)
    
    def on_current_selection_changed(self):
        selected = self.fileExplorer.selectedItems()
        if not selected:
            return
        
#         fulltextStart = 0
#         text = self.structuredFrame.textFrame.article_text.toPlainText()
#         fulltextEnd = len(text)



        item = selected[0]
        if not item.parent():
            item = item.child(0)
            
        if item:
#             label = self.fileExplorer.itemWidget(item, 0)
#             text = label.text()
#             basename = text.split()[0]
#             for i, rpt in enumerate(self.reports):
#                 if rpt.getfilename().startswith(basename):
#                     index = i
#                     break
            text = item.text(1)
            try:
                index = int(text)
            except:
                return
#             index = self.fileExplorer.indexOfTopLevelItem(item)
            report = self.reports[index]
            self.current_report = report
            self.mainFrame.set_current_report(report)
    
    def get_current_patient_reports(self, report):
        curptid = report.getPatientID()
        if curptid in self.patient_reports:
            return self.patient_reports[curptid]
        else:
            return []
    
#     def export_reports(self):
#         selectedItems = self.fileExplorer.selectedItems()
#         if not selectedItems:
#             return
#         
#         selected_reports = []
#         for item in selectedItems:
#             index = self.fileExplorer.indexOfTopLevelItem(item)
#             report = self.reports[index]
#             selected_reports.append(report)
#         
#         self.mainFrame.mainWindow.export_xls(selected_reports)
# 
#     def delete_report(self):
#         return

class ReportStatisticsFrame(QFrame):
    def __init__(self, reports = None, parent = None):
        super(ReportStatisticsFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
        
        self.setLayout(QVBoxLayout())
        
        grpTable = QGroupBox('Statistics')
        grpTable.setLayout(QVBoxLayout())
        self.tableData = QTableWidget()
        
        ##: Table initialization                
        self.header = ['Report', 'GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        self.tableData.setColumnCount(len(self.header))
        self.tableData.setHorizontalHeaderLabels(self.header)
        self.tableData.verticalHeader().hide()
        palette = self.tableData.horizontalHeader().setStyleSheet("QHeaderView::section {background-color:lightblue}")##: Populate data into the table
#         self.populateTableData(self.reports)
#         self.tableData.resizeColumnsToContents()
#         self.tableData.resizeRowsToContents()        
        
        grpTable.layout().addWidget(self.tableData)
        self.layout().addWidget(grpTable)
        
    
    def populateTableData(self, reports):
        self.tableData.clear()        
        self.tableData.setHorizontalHeaderLabels(self.header)
        self.tableData.setRowCount(len(reports) + 3)
        
        if len(reports)==0:
            return
        
        ##: Populate table data
        for i, report in enumerate(reports):
            row = report.getBHMs()
            self.populateTableRow([report.getFileName()] + row, i+3)
        
        ##: Calculate statistics
        ncol = len(self.header)-1
        data = []
        for i, report in enumerate(reports):
            row = report.getBHMValues()
            data.append(row)
        
        avgs = ['Average']
        stds = ["STD"]
        for i in range(ncol):
            coldata = [row[i] for row in data]
            avg = statistics.mean(coldata)
            if len(coldata)<=1:
                std = 0
            else:
                std = statistics.stdev(coldata)
            avgs.append("%4.2f"%(avg))
            stds.append("%4.2f"%(std))
        
        self.populateTableRow(avgs, 0)
        self.populateTableRow(stds, 1)
        
        self.tableData.resizeColumnsToContents()
            
    def populateTableRow(self, record, irow):
        """Populate the record to the table row"""
        for icol in range(len(record)):
            text = record[icol]
            if not isinstance(text, str):
                text = "%4.2f"%(text)
            ##: set the text columns
            item = QTableWidgetItem(text)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                
            self.tableData.setItem(irow, icol, item)


class PatientIndividualFrame(QFrame):
    def __init__(self, reports, parent = None):
        super(PatientIndividualFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
        self.setAutoFillBackground(True)
        
#         self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        
        if self.mainFrame.report_type == 'BHOP':
            self.setLayout(QHBoxLayout())
            self.frmTableData = PatientScoreTableFrame(self.reports, self.mainFrame)
            self.frmPatientPlot = PatientPlotFrame(self.reports, self.mainFrame)
        else:
            self.setLayout(QVBoxLayout())
            self.frmTableData = PatientPHQScoreTableFrame(self.reports, self.mainFrame)
            self.frmPatientPlot = PatientPHQPlotFrame(self.reports, self.mainFrame)
#         
#         self.frmBoxPlot = BoxPlotFrame(self.reports)
#         self.frmHistPlot = HistPlotFrame(self.reports)
        
#         frmPlots = QFrame()
#         frmPlots.setLayout(QVBoxLayout())
# #         frmPlots.layout().addWidget(self.frmBarPlot)
#         frmPlots.layout().addWidget(self.frmPatientPlot)
#         frmPlots.layout().addWidget(self.frmHistPlot)
        
        self.layout().addWidget(self.frmTableData)
        self.layout().addWidget(self.frmPatientPlot)
        self.layout().setStretch(0, 2)
        self.layout().setStretch(1, 3)
        
        self.update_report_list([])
    
    def set_current_report(self, report):
        patient_reports = self.mainFrame.frmFileExplorer.frmReportsPatients.get_current_patient_reports(report)
        self.update_report_list(patient_reports)
    
    
    def update_report_list(self, reports):
        self.reports = reports
#         fakereports = self.create_fake_reprots(100)
#         reports = self.reports + fakereports

        self.reports = reports
        self.frmPatientPlot.update_report_list(reports)
        self.frmTableData.populateTableData(reports)
    
    def update_report_type(self, report_type):
        self.reports = []
        QWidget().setLayout(self.layout())
        if report_type == 'BHOP':
            self.setLayout(QHBoxLayout())
            self.frmTableData = PatientScoreTableFrame(self.reports, self.mainFrame)
            self.frmPatientPlot = PatientPlotFrame(self.reports, self.mainFrame)
        else:
            self.setLayout(QVBoxLayout())
            self.frmTableData = PatientPHQScoreTableFrame(self.reports, self.mainFrame)
            self.frmPatientPlot = PatientPHQPlotFrame(self.reports, self.mainFrame)
            
        self.layout().addWidget(self.frmTableData)
        self.layout().addWidget(self.frmPatientPlot)
        self.layout().setStretch(0, 2)
        self.layout().setStretch(1, 3)
            
        
# class DashboardFrame(QFrame):
#     def __init__(self, parent = None):
#         super(DashboardFrame, self).__init__(parent)
#         self.mainFrame = parent
#         self.reports = self.mainFrame.reports
#         
#         self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
#                 
#         self.setLayout(QHBoxLayout())
#         self.frmTableBHM = ReportStatisticsFrame(self.reports)
#         self.frmBarPlot = BarPlotFrame(self.reports)
#         self.frmBoxPlot = BoxPlotFrame(self.reports)
#         self.frmHistPlot = HistPlotFrame(self.reports)
#         
#         frmPlots = QFrame()
#         frmPlots.setLayout(QVBoxLayout())
# #         frmPlots.layout().addWidget(self.frmBarPlot)
#         frmPlots.layout().addWidget(self.frmBoxPlot)
#         frmPlots.layout().addWidget(self.frmHistPlot)
#         
#         self.layout().addWidget(self.frmTableBHM)
#         self.layout().addWidget(frmPlots)
#         self.layout().setStretch(0, 3)
#         self.layout().setStretch(1, 2)
#         
#         self.update_report_list([])
        
class DashboardFrame(QSplitter):
    def __init__(self, parent = None):
        super(DashboardFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = self.mainFrame.reports
        
        self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
                
#         self.setLayout(QHBoxLayout())
        self.frmTableBHM = ReportStatisticsFrame(self.reports)
        self.frmBarPlot = BarPlotFrame(self.reports)
        self.frmBoxPlot = BoxPlotFrame(self.reports)
        self.frmHistPlot = HistPlotFrame(self.reports)
        
        frmPlots = QFrame()
        frmPlots.setLayout(QVBoxLayout())
#         frmPlots.layout().addWidget(self.frmBarPlot)
        frmPlots.layout().addWidget(self.frmBoxPlot)
        frmPlots.layout().addWidget(self.frmHistPlot)
        
        self.addWidget(self.frmTableBHM)
        self.addWidget(frmPlots)
#         self.setStretchFactor(0, 3)
#         self.setStretchFactor(1, 2)
        self.frmBoxPlot.sizePolicy().setHorizontalStretch(3)
        self.frmHistPlot.sizePolicy().setHorizontalStretch(2)
        sizeHint = self.sizeHint()
        sizes = [0.55 * sizeHint.width(), 0.45 * sizeHint.width()]
#             QList<int> Sizes;
#     Sizes.append(0.5 * sizeHint().height());
#     Sizes.append(0.5 * sizeHint().height());
        self.setSizes(sizes)
    

        self.update_report_list([])
    
    def create_fake_reprots(self, num):
        data = {}
        for scr in self.scores:
            mn = random.random()*4
            vals = [random.gauss(mn, 0.5) for i in range(num)]
            vs = []
            for v in vals:
                if v<0:
                    nv = 0
                elif v>4:
                    nv = 4
                else:
                    nv = v
                vs.append(nv)
            data[scr] = vs
        
        reports = []
        for i in range(num):
            scores = ["%4.2f"%(data[scr][i]) for scr in self.scores]
            report = Report()
            report.setBHMs('made-up #'+str(i), scores)
            
            reports.append(report)
        return reports
    
    def update_report_list(self, reports):
        self.reports = reports
#         fakereports = self.create_fake_reprots(100)
#         reports = self.reports + fakereports

        self.reports = reports
        self.frmTableBHM.populateTableData(reports)
        self.frmBarPlot.update_report_list(reports)
        self.frmBoxPlot.update_report_list(reports)
        self.frmHistPlot.update_report_list(reports)
        
class BarPlotFrame(QGroupBox):
    def __init__(self, reports, parent = None):
        super(BarPlotFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QHBoxLayout())
        self.setTitle('Mean')
        
#         grpBarPlot = QGroupBox('Mean')
#         grpBarPlot.setLayout(QVBoxLayout())

        ##: Table initialization                
        self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        
        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        self.layout().addWidget(self.canvas)
        
#         self.layout().addWidget(grpBarPlot)
        
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
        
    def plot(self):
        ''' plot some random stuff '''

        data = []
        for report in self.reports:
            BHMs = report.getBHMValues()
            data.append(BHMs)
            
        avgs = []
        for i, score in enumerate(self.scores):
            coldata = [row[i] for row in data]
            if len(coldata)==0:
                avg = 0
            elif len(coldata)==1:
                avg = coldata[0]
            else:
                avg = statistics.mean(coldata)
            avgs.append(avg)
            
        self.ax.cla()
        x = [i for i in range(len(self.scores))]
        y = avgs
        width = .5
        self.ax.bar(x, y, width, color='C2')
        self.ax.set_xticks(x)
        self.ax.set_xticklabels(self.scores,  fontsize=8)
        self.ax.set_ylabel('Mean')
        self.ax.set_xlabel('Behavioral Health Measure-20')

#         self.figure.tight_layout()
        # refresh canvas
        self.canvas.draw()


class PatientScoreTableFrame(QGroupBox):
    def __init__(self, reports = None, parent = None):
        super(PatientScoreTableFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
        
        self.setTitle('Historical Data')
        self.setLayout(QVBoxLayout())
        

        self.tableScore = QTableWidget()
        
        ##: Table initialization                
        self.header = ['Date', 'GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        self.tableScore.setColumnCount(len(self.header))
        self.tableScore.setHorizontalHeaderLabels(self.header)
        self.tableScore.verticalHeader().hide()
        palette = self.tableScore.horizontalHeader().setStyleSheet("QHeaderView::section {background-color:lightblue}")##: Populate data into the table
#         self.populateTableData(self.reports)
#         self.tableData.resizeColumnsToContents()
#         self.tableData.resizeRowsToContents()        
        
        self.layout().addWidget(self.tableScore)
        
    
    def populateTableData(self, reports):
        self.tableScore.clear()        
        self.tableScore.setHorizontalHeaderLabels(self.header)
        self.tableScore.setRowCount(len(reports))
        
        if len(reports)==0:
            return
        
        ##: Populate table data
        for i, report in enumerate(reports):
            row = report.getBHMs()
            dt = report.getDate().isoformat()
            self.populateTableRow([dt] + row, i)
        
#         ##: Calculate statistics
#         ncol = len(self.header)-1
#         data = []
#         for i, report in enumerate(reports):
#             row = report.getBHMValues()
#             data.append(row)
        
        self.tableScore.resizeColumnsToContents()
            
    def populateTableRow(self, record, irow):
        """Populate the record to the table row"""
        for icol in range(len(record)):
            text = record[icol]
            if not isinstance(text, str):
                text = "%4.2f"%(text)
            ##: set the text columns
            item = QTableWidgetItem(text)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                
            self.tableScore.setItem(irow, icol, item)


class PatientPlotFrame(QGroupBox):
    def __init__(self, reports, parent = None):
        super(PatientPlotFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QVBoxLayout())
        self.setTitle('Measures')
        self.setAutoFillBackground(True)
        
#         grpHistPlot = QGroupBox('Distribution')
#         grpHistPlot.setLayout(QVBoxLayout())

        
        ##: Table initialization                
        self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        
        self.cbxHBMs = QComboBox()
        self.cbxHBMs.addItems(self.scores)
        self.cbxHBMs.currentIndexChanged.connect(self.on_current_score_changed)
        
        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        frmHBMS = QFrame()
        frmHBMS.setLayout(QHBoxLayout())
        frmHBMS.layout().addWidget(QLabel("Behavioral Health Measure-20:"))
        frmHBMS.layout().addWidget(self.cbxHBMs)
        frmHBMS.layout().addWidget(QLabel(''))
        frmHBMS.layout().setStretch(2,10)
        self.layout().addWidget(frmHBMS)
        self.layout().addWidget(self.canvas)
        
        self.frmBHMplot = PatientBHMScoreFrame(self.reports, self.mainFrame)
        self.layout().addWidget(self.frmBHMplot)
        
        self.current_BHM_index = 0
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
        self.frmBHMplot.update_report_list(reports)
    
    def on_current_score_changed(self):
        self.current_BHM_index = self.cbxHBMs.currentIndex()
        self.plot()
        
    def plot(self):
        if len(self.reports)==0:
            return
        
        score = self.scores[self.current_BHM_index]
        data = [report.getBHMValues()[self.current_BHM_index] for report in self.reports]
        
#         ax = self.figure.add_subplot(111)
        # the histogram of the data
        x = [report.getDate() for report in self.reports]
        
        self.ax.cla()
        self.ax.plot(x, data)
        self.ax.set_ylim([1, 4])
        self.ax.set_xlabel('Date')
        self.ax.set_ylabel('Score')

        self.ax.grid(True)

        self.canvas.draw()
        
class PatientBHMScoreFrame(QFrame):
    def __init__(self, reports, parent = None):
        super(PatientBHMScoreFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QVBoxLayout())
#         self.setTitle('Measures')
        
#         grpHistPlot = QGroupBox('Distribution')
#         grpHistPlot.setLayout(QVBoxLayout())

        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)


        self.canvas = FigureCanvas(self.figure)

        self.layout().addWidget(self.canvas)
        
        self.current_BHM_index = 0
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
    
    def on_current_score_changed(self):
        self.current_BHM_index = self.cbxHBMs.currentIndex()
        self.plot()
        
    def plot(self):
        if len(self.reports)==0:
            return
        
#         scores = [self.BHM20_Global_Mental_Health_Scale_Score, self.BHM20_Well_Score, 
#                   self.BHM20_Psychological_Symptoms_Score, self.BHM20_Life_Functioning_Score]
        
        BHM20Ans = ["Normal", "Mild Distress", "Moderate Distress", "Severe Distress"]
        
        BHM20Scores = ["BHM-20 Global Mental Health Scale Score", "BHM-20 Well- Score", 
                       "BHM-20 Psychological Symptoms Score", "BHM-20 Life Functioning Score"]
        
        colors = ['yellow', 'red', 'blue', 'darkcyan', 'cyan', 'skyblue', 'green',  'magenta',  'gray',  'lightGray',
                  'lightblue', 'lightgreen']

        data = [report.getBHMScores() for report in self.reports]
        
#         ax = self.figure.add_subplot(111)
        # the histogram of the data
        x = [report.getDate() for report in self.reports]
        
        self.ax.cla()
        for i, score in enumerate(BHM20Scores):
            y = [BHM20Ans.index(row[i])+1 for row in data]
            self.ax.plot(x, y, color=colors[i], label=BHM20Scores[i])
            
        self.ax.set_ylim([0, 5])
        self.ax.set_xlabel('Date')
#         self.ax.set_ylabel('Score')
        self.ax.set_yticks([0, 1, 2, 3, 4])
        
        self.ax.set_yticklabels(['',"Normal", "Mild", "Moderate", "Severe"], rotation=45)
        leg = self.ax.legend(loc='upper left', fontsize=8)
        leg.get_frame().set_alpha(0.2)
        self.ax.set_title('BHM20 Distress Scores')
        self.ax.grid(True)

        self.canvas.draw()
        
class BoxPlotFrame(QGroupBox):
    def __init__(self, reports, parent = None):
        super(BoxPlotFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QHBoxLayout())
        self.setTitle('Boxplot')
        
#         grpBarPlot = QGroupBox('Mean')
#         grpBarPlot.setLayout(QVBoxLayout())

        ##: Table initialization                
        self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        
        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        self.layout().addWidget(self.canvas)
        
#         self.layout().addWidget(grpBarPlot)
        
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
        
    def plot(self):
        ''' plot some random stuff '''
        if len(self.reports)==0:
            return
        
        data = []
        for report in self.reports:
            BHMs = report.getBHMValues()
            data.append(BHMs)
        
        tdata = []
        
        for i, score in enumerate(self.scores):
            coldata = [row[i] for row in data]
            tdata.append(coldata)
            
        # create an axis
#         ax = self.figure.add_subplot(111)
        x = [i for i in range(len(self.scores))]
        width = .5
#         self.ax.bar(x, y, width, color='C2')
        
        self.ax.cla()
        bplot = self.ax.boxplot(tdata, patch_artist=True)
#         self.ax.violinplot(tdata, showmeans=False, showmedians=True)
        
        self.ax.set_xticks([p+1 for p in x])
        self.ax.set_xticklabels(self.scores,  fontsize=8)
        self.ax.set_ylabel('Values')
        self.ax.set_xlabel('Behavioral Health Measure-20')
        
        colors = ['yellow', 'red', 'skyblue', 'green', 'blue', 'cyan', 'magenta',  'gray', 'darkcyan',  'lightGray',
                  'lightblue', 'lightgreen']
        for i, box in enumerate(bplot['boxes']):
#             patch.set_facecolor(colors[i])
            box.set(facecolor = colors[i])
#         self.figure.tight_layout()
        # refresh canvas
        self.canvas.draw()
    
        
class HistPlotFrame(QGroupBox):
    def __init__(self, reports, parent = None):
        super(HistPlotFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = reports
                
        self.setLayout(QVBoxLayout())
        self.setTitle('Distribution')
        
#         grpHistPlot = QGroupBox('Distribution')
#         grpHistPlot.setLayout(QVBoxLayout())

        
        ##: Table initialization                
        self.scores = ['GMH', 'Well', 'Psy', 'LFS', 'Anx', 'Dep', 'ETOH', 'Harm', 'SI']
        
        self.cbxHBMs = QComboBox()
        self.cbxHBMs.addItems(self.scores)
        self.cbxHBMs.currentIndexChanged.connect(self.on_current_score_changed)
        
        # a figure instance to plot on
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        frmHBMS = QFrame()
        frmHBMS.setLayout(QHBoxLayout())
        frmHBMS.layout().addWidget(QLabel("Behavioral Health Measure-20:"))
        frmHBMS.layout().addWidget(self.cbxHBMs)
        frmHBMS.layout().addWidget(QLabel(''))
        frmHBMS.layout().setStretch(2,10)
        self.layout().addWidget(frmHBMS)
        self.layout().addWidget(self.canvas)
        
        self.current_BHM_index = 0
        self.plot()
    
    def update_report_list(self, reports):
        self.reports = reports
        self.plot()
    
    def on_current_score_changed(self):
        self.current_BHM_index = self.cbxHBMs.currentIndex()
        self.plot()
        
    def plot(self):
        if len(self.reports)==0:
            return
        
        score = self.scores[self.current_BHM_index]
        data = [report.getBHMValues()[self.current_BHM_index] for report in self.reports]
        
#         ax = self.figure.add_subplot(111)
        # the histogram of the data
        
        self.ax.cla()
        num_bins = 10       
        n, bins, patches = self.ax.hist(data, num_bins, edgecolor="k")
        
#         # add a 'best fit' line
#         y = mlab.normpdf(bins, mu, sigma)
#         ax.plot(bins, y, '--')
#         self.ax.set_xlabel(score)
        self.ax.set_xlabel(score)
        self.ax.set_ylabel('Count')
        self.ax.set_xlim([0, 4])
        
#         ax.set_title(r'Histogram of IQ: $\mu=100$, $\sigma=15$')
        
        # Tweak spacing to prevent clipping of ylabel
#         self.figure.tight_layout()

        self.ax.grid(True)

        self.canvas.draw()
        

class OptionDialog(QDialog):
    def __init__(self, options={}, parent = None):
        super(OptionDialog, self).__init__(parent, )        
        self.options = options
        
        self.setWindowTitle('Assessment')
        self.setFixedHeight(300)
        self.setFixedWidth(500)
        self.setLayout(QVBoxLayout())

        frmThreshold = QGroupBox('PHQ-9')
        frmThreshold.setLayout(QHBoxLayout())
        self.chkThreshold = QCheckBox('PHQ-9 Score Threshold:')
        self.chkThreshold.stateChanged.connect(self.on_chk_threshold_changed)
        self.txtThreshold = QLineEdit('20')
        self.txtThreshold.setFixedWidth(30)
        self.txtThreshold.setDisabled(True)
        frmThreshold.layout().addWidget(self.chkThreshold)
        frmThreshold.layout().addWidget(self.txtThreshold)
        frmThreshold.layout().addWidget(QLabel(''))
        frmThreshold.layout().setStretch(2,100)
        
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        
        self.layout().addWidget(frmThreshold)
        self.layout().addWidget(QLabel(''))
        self.layout().addWidget(buttonBox)
        self.layout().setStretch(0, 1)
        self.layout().setStretch(1, 20)
        self.layout().setStretch(2, 1)
        
        self.populate_options(self.options)
#
    def populate_options(self, options):    
        if "PHQ-9" in options and options['PHQ-9']>=0:
            self.chkThreshold.setChecked(True)
            self.txtThreshold.setEnabled(True)
            self.txtThreshold.setText(str(options['PHQ-9']))
        else:
            self.chkThreshold.setChecked(False)
            self.txtThreshold.setDisabled(True)
            
            
    def on_chk_threshold_changed(self):
        if self.chkThreshold.isChecked():
            self.txtThreshold.setEnabled(True)
        else:
            self.txtThreshold.setDisabled(True)
            
    def accept(self):
        if self.chkThreshold.isChecked() and str(self.txtThreshold.text()).isdigit():
            self.options['PHQ-9'] = int(self.txtThreshold.text())
        else:
            self.options['PHQ-9'] = None

        self.close()
    
    def getOptions(self):
        return self.options

class ReportTextFrame(QFrame):
    def __init__(self, parent = None):
        super(ReportTextFrame, self).__init__(parent)
        self.mainFrame = parent
        self.reports = self.mainFrame.reports
        
        self.setAutoFillBackground(True)
        
        self.setLayout(QVBoxLayout())
        self.txtReportText = QTextEdit()
        self.layout().addWidget(self.txtReportText)
    
    def set_current_report(self, report):
        self.txtReportText.setText(report.getText())
        
class MainFrame(QSplitter):
    def __init__(self, reports, parent = None):
        super(MainFrame, self).__init__(parent)
        
        self.reports = reports
        self.mainWindow = parent
        self.report_type = self.mainWindow.report_type
#         self.setLayout(QHBoxLayout())
        
        self.frmFileExplorer = FileExplorerFrame(reports, self)
        
        if self.report_type=="BHOP":
            self.frmReport = ReportFrame(self)
        else:  ##: PHQ-9
            self.frmReport = ReportPHQ9Frame(self)
        
        self.frmReportText = ReportTextFrame(self)
        self.frmPatient = PatientIndividualFrame([], self)
        
        self.reportTabs = QTabWidget(self)
        self.reportTabs.addTab(self.frmReport, 'Info')
        self.reportTabs.addTab(self.frmReportText, 'Text')
        
        self.reportTabs.addTab(self.frmPatient, 'History')
        self.reportTabs.removeTab(2)  ##: A workaround for the misplace of frmPatient, which is still a mystery.
        
#         self.frmReportStat = ReportStatisticsFrame(self)
#         self.frmReportStat.tableData.setAutoFillBackground(True)
        
        self.frmReport.setAutoFillBackground(True)
        self.stackedWidget = QStackedWidget()
        self.stackedWidget.addWidget(self.reportTabs)
        
        if self.report_type=="BHOP":
            self.frmDashboard = DashboardFrame(self)
            self.stackedWidget.addWidget(self.frmDashboard)
        self.stackedWidget.setCurrentIndex(0)
        
        self.addWidget(self.frmFileExplorer)    
        self.addWidget(self.stackedWidget)
        self.setStretchFactor(1,4)
#         self.addWidget(self.frmReport)
        
#         self.frmReport.set_current_report(self.reports[0])
#         self.layout().addWidget(frmFileExplorer)    
#         self.layout().addWidget(frmReport)

    def get_sys_options(self):
        return self.mainWindow.options
    
    def set_current_report(self, report):
        self.frmReport.set_current_report(report)
        self.frmReportText.set_current_report(report)
        self.frmPatient.set_current_report(report)

    def add_report(self, report):
        self.frmFileExplorer.add_report(report)
        self.frmReport.set_current_report(report)
        
    def update_report_list(self, reports):
        self.reports = reports
        self.frmFileExplorer.reports = reports            
        self.frmFileExplorer.populate_reports(reports)
        self.frmReport.reports = reports
        self.frmReportText.reports = reports
        
        if self.report_type=="BHOP":
            self.frmDashboard.update_report_list(reports)
        
#         self.frmReportStat.reports = reports
#         self.frmReportStat.populateTableData(reports)
        
    def update_report_type(self, report_type):
        
        if self.report_type == report_type:
            return
        
        self.mainWindow.reports = []
        self.update_report_list([])
        self.report_type = report_type
        if self.report_type=="BHOP":
            self.frmReport = ReportFrame(self)
        else:  ##: PHQ-9
            self.frmReport = ReportPHQ9Frame(self)
            
        self.frmReport.setAutoFillBackground(True)
        self.reportTabs.removeTab(0)
        self.reportTabs.insertTab(0, self.frmReport, "Info")
        self.reportTabs.setCurrentIndex(0)
        

        self.frmPatient.update_report_type(report_type)
        self.frmPatient.setAutoFillBackground(True)
        if self.reportTabs.count()==3:
            self.reportTabs.removeTab(2)
            self.reportTabs.insertTab(2, self.frmPatient, "History")
        else:
            self.reportTabs.addTab(self.frmPatient, 'History')
            self.reportTabs.removeTab(2) 
        
        self.stackedWidget.setCurrentIndex(0)

        
#         self.reportTabs = QTabWidget(self)
#         self.reportTabs.addTab(self.frmReport, 'Info')
#         self.reportTabs.addTab(self.frmReportText, 'Text')
#         
#         self.reportTabs.addTab(self.frmPatient, 'History')
#         self.reportTabs.removeTab(2)  ##: A workaround for the misplac
#         
        
class MainWindow(QMainWindow):
    """This class is for the system main window"""
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        
#         self.setWindowTitle("Mental Health Data Analyzer")
#         self.resize(1200, 800)    
#         dbfilename = './alterationsdb.sqlite'
#         os.remove(dbfilename)
        
        
#         self.reportParser = ReportParser()
        

        self.report_type = 'BHOP'
#         self.report_type = 'PHQ-9'
        self.setWindowTitle("Mental Health Data Analyzer (" + self.report_type + " Report)")
        self.options = {}
        
        self.reports = []
        
#         report2 = Report('./data/fake_reports/patient#01_04_02_2000.docx')
#         report1 = Report('./data/fake_reports/patient#01_06_11_2006.docx')
#         report3 = Report('./data/fake_reports/patient#01_07_28_2002.docx')
#         report4 = Report('./data/fake_reports/patient#02_08_22_2008.docx')
#         self.reports = [report1, report2, report3, report4]
# 
#         report1 = ReportPHQ('./data/fake_PHQ_reports/patient#01_01_20_2006.docx')
#         report2 = ReportPHQ('./data/fake_PHQ_reports/patient#01_06_10_1998.docx')
#         report3 = ReportPHQ('./data/fake_PHQ_reports/patient#01_07_28_2008.docx')
#         report4 = ReportPHQ('./data/fake_PHQ_reports/patient#03_07_11_2011.docx')
#         self.reports = [report1, report2, report3, report4]
        
        self.mainFrame = MainFrame(self.reports, self)
        
        self.setCentralWidget(self.mainFrame)        
#         self.showMaximized()
        self.setWindowState(Qt.WindowMaximized)
#         self.showFullScreen()

        ##: Create File Menu        
        self.load_action = QAction(QIcon('openfile.jpg'), "Load...", self)
        self.load_action.setShortcut("Ctrl+L")
        self.load_action.setStatusTip("Load data files")
        self.load_action.triggered.connect(self.load_report_file)
        
        self.folder_action = QAction(QIcon('openfolder.jpg'), "Load Folder...", self)
        self.folder_action.setShortcut("Ctrl+S")
        self.folder_action.setStatusTip("Load reports in a folder")
        self.folder_action.triggered.connect(self.load_report_folder)
        
        self.browse_action = QAction("Browse Database", self)
        self.browse_action.setShortcut("Ctrl+D")
        self.browse_action.setStatusTip("Browse reports in the database")
#         self.save_action.triggered.connect(self.save_database)


        self.export_action = QAction(QIcon('export.jpg'), "Export...", self)
        self.export_action.setShortcut("Ctrl+E")
        self.export_action.setStatusTip("Export results into an Excel file")
        self.export_action.triggered.connect(self.export_xls)
        
        

        self.exit_action = QAction("Exit", self)
        self.exit_action.setShortcut("Ctrl+Q")
        self.exit_action.setStatusTip("Exit application")
        self.exit_action.triggered.connect(self.close)
        
        self.menu_separator = QAction(self)
        self.menu_separator.setSeparator(True)
        self.menu_separator2 = QAction(self)
        self.menu_separator2.setSeparator(True)
        self.menu_separator3 = QAction(self)
        self.menu_separator3.setSeparator(True)
        self.menu_separator4 = QAction(self)
        self.menu_separator4.setSeparator(True)
        
        self.file_menu = self.menuBar().addMenu("File")
        self.file_menu.addAction(self.load_action)         
        self.file_menu.addAction(self.folder_action)        
        self.file_menu.addAction(self.browse_action)               
        self.file_menu.addAction(self.menu_separator)          
        self.file_menu.addAction(self.export_action)                
        self.file_menu.addAction(self.menu_separator2)             
        self.file_menu.addAction(self.exit_action)      
        
        
        self.analysis_menu = self.menuBar().addMenu("Analysis")
        self.stat_action = QAction(QIcon('statistics.png'), "Statistic Analysis", self)
#         self.stat_action = QAction("Statistic Analysis...", self)
        self.stat_action.setStatusTip("Statistic Analysis")
        self.stat_action.triggered.connect(self.toggle_statistic_analysis)        
        self.stat_action.setCheckable(True)
        
        self.option_action = QAction("Assessment...", self)
        self.option_action.triggered.connect(self.open_option_dialog)   
        
        self.analysis_menu.addAction(self.stat_action)   
        self.analysis_menu.addAction(self.option_action)      
        
        self.report_menu = self.menuBar().addMenu("Report")
        self.BHOP_action = QAction("BHOP", self)
        self.PHQ_action = QAction("PHQ-9", self)
#         self.stat_action = QAction("Statistic Analysis...", self)
        self.BHOP_action.setStatusTip("BHOP Report Analysis")
        self.PHQ_action.setStatusTip("PHQ-9 Report Analysis")
        self.BHOP_action.triggered.connect(self.toggle_report_type_BHOP)        
        self.PHQ_action.triggered.connect(self.toggle_report_type_PHQ)        
        self.BHOP_action.setCheckable(True)       
        self.PHQ_action.setCheckable(True)
        self.report_menu.addAction(self.BHOP_action)
        self.report_menu.addAction(self.PHQ_action)
        if self.report_type == 'BHOP':
            self.BHOP_action.setChecked(True)
        else:
            self.PHQ_action.setChecked(True)
        
        
        ##: Create Help Menu        
        self.help_action = QAction("User Manual", self)
#         self.help_action.triggered.connect(self.showContents)
        self.about_action = QAction("About", self)
#         self.about_action.triggered.connect(self.showAbout)
        self.help_menu = self.menuBar().addMenu("Help")
        self.help_menu.addAction(self.help_action)        
        self.help_menu.addAction(self.about_action)    
        
        ##: Create Toolbar for menu items
        toolbarFile = self.addToolBar("Files")
        toolbarFile.addAction(self.load_action)
        toolbarFile.addAction(self.folder_action)
        toolbarFile.addAction(self.menu_separator3)
        toolbarFile.addAction(self.export_action)
        toolbarFile.addAction(self.menu_separator4)
        toolbarFile.addAction(self.stat_action)
        
        if len(self.reports)>0:
            self.mainFrame.set_current_report(self.reports[0])
    
    def toggle_report_type_BHOP(self):
        self.PHQ_action.setChecked(False)
        self.BHOP_action.setChecked(True)
        self.stat_action.setEnabled(True)
        self.report_type = "BHOP"
        self.setWindowTitle("Mental Health Data Analyzer (" + self.report_type + " Report)")
        
        self.mainFrame.update_report_type(self.report_type)
#         self.mainFrame = MainFrame(self.reports, self)
#         self.setCentralWidget(self.mainFrame)      
            
        self.setAutoFillBackground(True)
        
    def toggle_report_type_PHQ(self):
#         if self.BHOP_action.isChecked():
#             self.PHQ_action.setChecked(False)
#             self.BHOP_action.setChecked(True)
#             self.report_type = "BHOP"
#         elif self.PHQ_action.isChecked():
#             self.BHOP_action.setChecked(False)
#             self.PHQ_action.setChecked(True)
#             self.report_type = "PHQ-9"

        self.BHOP_action.setChecked(False)
        self.PHQ_action.setChecked(True)
        self.stat_action.setDisabled(True)
        self.report_type = "PHQ-9"
                        
        self.setWindowTitle("Mental Health Data Analyzer (" + self.report_type + " Report)")
        self.mainFrame.update_report_type(self.report_type)
        
#         self.mainFrame = MainFrame(self.reports, self)
#         self.setCentralWidget(self.mainFrame)      
    
    def open_option_dialog(self):
        dlg = OptionDialog(self.options)     
        dlg.exec_()
        self.options = dlg.getOptions()
        
#         if "PHQ-9" in options and "PHQ-9" in self.options and options["PHQ-9"]!=self.options["PHQ-9"]:
#         if "PHQ-9" in options and self.options['PHQ-9']!=None:
#             self.options = options
#             
        self.mainFrame.frmPatient.frmPatientPlot.plot()
        self.mainFrame.frmFileExplorer.frmReportsAll.populate_reports(self.reports)
        self.mainFrame.frmFileExplorer.frmReportsPatients.populate_reports(self.reports)
        
    
    def toggle_statistic_analysis(self):
        if self.stat_action.isChecked():
            self.mainFrame.stackedWidget.setCurrentIndex(1)
        else:
            self.mainFrame.stackedWidget.setCurrentIndex(0)
            
    def load_report_folder(self):
        fdir = QFileDialog.getExistingDirectory(self, "Load reports from selected folder", './', 
                                                QFileDialog.ShowDirsOnly|QFileDialog.DontResolveSymlinks)
        if not fdir:
            return
        filenames = os.listdir(fdir)
        reportnames = [fdir+'/'+fn for fn in filenames if not fn.startswith('~') and (fn.endswith('docx') or fn.endswith('doc'))]
#         reportnames = [str(fn.replace('\\','/')) for fn in reportnames]
        reportnames = [str(fn) for fn in reportnames]
        reports = self.process_all_reports(reportnames)
        self.reports += reports 
        if len(self.reports) > 0:
            self.mainFrame.update_report_list(self.reports)
            self.mainFrame.set_current_report(self.reports[0])
                       
    def load_report_file(self):
        """Load report file"""
        ##: Get file name
        filenames = QFileDialog.getOpenFileNames(self, "Load reports", './', "Data File (*.pdf *.docx *.doc)")       
        if not filenames: 
            return   
        
        fNames = [str(fn) for fn in filenames] 
        reports = self.process_all_reports(fNames)
        self.reports += reports 
        if len(self.reports) > 0:
            self.mainFrame.update_report_list(self.reports)
            self.mainFrame.set_current_report(self.reports[0])
         
#         for filename in filenames:
#             report = Report(filename)
#             report = self.reportParser.parse(report)
#             self.reports.append(report)
#             self.mainFrame.add_report(report)
        
    def write_sheet_row(self, sheet, datarow, irow):
        for i, c in enumerate(datarow):
            sheet.write(irow, i, c)
        
    def export_xls(self, reports = []):
        if not reports:
            reports = self.reports
            
        filename = QFileDialog.getSaveFileName(self, "Save reports in a .xls file", "./data/reports_exported.xls", "xls (*.xls)")
        if not filename: return
        
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        
        workbook = xlwt.Workbook()
        
        if self.report_type=="BHOP":
            header = ['File Name', 'GMH','Well','Psy','LFS', 'Anx', 'Dep','ETOH','Harm', 'SI']
            sheet = workbook.add_sheet("BHM20 Measures")
        else:
            header = ['File Name','Score'] + ['Q'+str(i+1) for i in range(9)] + ["Difficulty"]
            sheet = workbook.add_sheet("PHQ-9")
            
        ##: Write test information sheet
        self.write_sheet_row(sheet, header, 0)
        
        if self.report_type=="BHOP":
            for irow, report in enumerate(reports):
                self.write_sheet_row(sheet, [report.getFileName()] + report.getBHMs(), irow+1)
        else:
            for irow, report in enumerate(reports):
                self.write_sheet_row(sheet, [report.getFileName(), report.getScore()] + report.getPHQ9Ans() + [report.getDifficulty()], irow+1)
            
        workbook.save(filename)         
        
        QApplication.restoreOverrideCursor()       

    def process_all_reports(self, filenames):
        
        numreports = len(filenames)
        
        progress = QProgressDialog("Extracting data from {0} reports.".format(numreports), "Cancel", 0, numreports - 1)
        progress.setMinimumWidth(500)
        progress.setWindowTitle("Mental Health Data Extraction")
        progressbar = QProgressBar(progress)
        progressbar.setVisible(1)
        progress.setBar(progressbar)
        progress.setWindowModality(Qt.WindowModal)
        progress.setRange(0, numreports)
        progress.setValue(0)
        reports =[]
        for idx, filename in enumerate(filenames):
            if self.report_type=='BHOP':
                report = Report(filename)
            else:
                report = ReportPHQ(filename)
                
            reports.append(report)
            self.mainFrame.frmFileExplorer.add_report(report)
            
            progress.setValue(idx)
            continue            
             
            if progress.wasCanceled():
                #sys.exit()  
                return []
                     
        return reports
    
if __name__ == '__main__':
    
    app = QApplication(sys.argv)

    main_window = MainWindow()
    main_window.show()
    
    sys.exit(app.exec_())