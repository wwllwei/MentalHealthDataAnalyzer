Behavioral Health Consultation
" Final Report '

* Final Report *

Admin

Patient referred by: Self

Source of information: Oﬂier: NP Julagay

Feedback given to PCM: Y

Patient seen by trainee: N

Patient was seen for 30 minute IBHC appointment: Y
Pathways: _

IBHC visit number: 1

Patient given IBHC brochure: Y

Chief Complaint: anxiety

Health Habits:
Pain (0-10): 0
Current Meds (relevant to problem): none specific to issue
Caffeine use: 1-2 drinks per day
Tobacco use: vaps and chews

EFOH: no
Deployment Related? no

ﬂghavioral Health Measure-20: (Only for IBHC provider use as per contract)
07/09/2018 GMH — 3.35 Well - 3.00 Psy - 3.54 LFS - 3.00 Anx - 2.75 Dep - 3.67 l:TOH - 4.00 Harm - 4.00 SI
- 4.00

BHM-20 Global Mental Health Scale Score: Normal
BHM-20 Well- Score: Normal

BHM-20 Psychological Symptoms Score: Normal
BHM-20 Life Functioning Score: Normal

C-SSRS CDS

In the past month have you?
1: Have you wished you were dead or wished you could go to sleep and not wake up? No

2: Have you had any actual thoughs of killing yourself? No (skip to question #6)

Result type: Behavioral Health Consultation

Result date: July 09, 2018 13:06 PDT

Result status: Auth (Verified)

Result title: BHOP

Perfonned by: WILLIAMS, JEFFREY K on July 09, 2018 13:13 PDT

Veriﬁed by: WILLIAMS, JEFFREY K on July 11, 2018 14:35 PDT

Encounter info: 1608626, 0128C, Clinic, 07/09/2018 - 07/12/2018

Printed by: WILLIAMS, JEFFREY K Page 1 of 4
(Continued)

Printed on: 07/12/18 09:10 PDT

‘Behavioral Health Consultation

. Final Report ‘

3: Have you been thinking about how you might do this? No
4; Have you had these thoughts and had some Intention of acting on them? No
5: Have you started to work out or worked out the details of how to kill yourself? Do you intend to carry out this plan? No

In your lifetime have you?
6a: Have you ever done anything, started to do anything, or prepared to do anything to end your life? No

6b: Within the last three months? No

C-SSRS Summag:
Patient was screened for suicide risk using the C»SSRS.No suicide risk items were endorsed.

Actions taken: no action required

IBHC Review of Systems

Systemic: Not feeling tired (fatigue).

Gastrointestinal: No change in appetite.

Neurological: No disorientation.

Psychological: No sleep disturbances. Normal enjoyment of activities and no impulsive behavior

IBHC Mental Status Exam
The patient reported: No decrease in concentrating ability.
Psychological: Not thinking about suicide, not having a suicide plan, and no stated intent to commit suicide. No homicidal

thoughts, not thinking of a way to do it, and without a stated intent to kill.
General Appearance: Alert. Well developed. Well nourished. In no acute distress.
Neurological: No disorientation was observed - oriented to place, person, time, and situation. No hallucinations. Memory was
unimpaired. Remote memory was not impaired . Recent memory was not impaired. Judgement was not impaired.
Speech: Rate was normal. Rate was not slowed. Not pressured. Tone was not monotone. Volume was normal. No articulation
abnormalities. No language abnormalities were demonstrated.
Demonstrated Behavior: No decreased eye-to-eye contact was observed.
Attitude: Cooperative.
Mood: Anxious
Affect: Normal. Not labile. Not ﬂat. Not constricted. Showed no irritability. Congruent with mood.
Thought Processes: Not impaired - they were linear, logical, and goal directed. Evaluation of connectedness showed no
deﬁciency. Attention demonstrated no abnormalities.
Thought Content: Insight was intact. No delusions.

Result type: Behavioral Health Consultation

Result date: July 09, 2018 13:06 PDT

Result status: Auth (Verified)

Result title: BHOP

Performed by: WILLIAMS, JEFFREY K on July 09, 2018 13:13 PDT
Verified by: WILLIAMS, JEFFREY K on July 11, 2018 14:35 PDT

Encounter info: 1608626, 0128C, Clinic, 07/O9/2018 — 07/12/2018

Printed by: WlLLlAMS, JEFFREY K Page 2 of 4
Printed on: 07/12/18 09:10 PDT (Continued)

  
  

ehavioral Health Consultation
. Final Rel-’°“ *

15 othr robl his of r n ill sCDS

Description of Symptoms: worries, pessimistic about the future, passed out during recent PT test, some heart racing, shortness of

breath
Duration: 6-7 years, increased over past several months

ctors correlated with onset: transitioning to Army, concerned things might go wrong, PT test

Fa
Frequency of sx: several times per week

Severity of Sx: mild
Psychosocial factors: working on transitioning from Air Force to Army (end of August), married (good relationship), one child 19

months. Stress about transitioning steps. Some ﬁnancial worries. Support - wife, co—workers. Enjoys — play music. Exercises

regularly.
Aggravating factors: worrying
Alleviating fazxors: relaxation breathing, music

Current tx: none
Past tx: medication (not currently taking)

Functional impact: work, personal wellbeing,

IBHC other Jioblem intervention and patient education

Other problem interventions: _

gssment and Plan

Stress
Dr Chen, this Pt was referred to IBHC by NP Julagay for anxiety issues. Pt reported symptoms of heightened stress response and
anxiousness over the past 6-7 years, but has noticed an inc ease in severity and frequency over the past several months.

Contributing factors appear to be Pt's pending transfer from Air Force to Army, taking PT test and his cognitive pattern of
does appear to have some insight into his distorted thoughts and some

excessive worry (preparing for things to go wrong). Pt
awareness of basic symptoms management strategies. Pt and XBHC agreed upon the following plan.

Other problem recommendations for patient:
1) Pt provided with verbal and written information on stress management and worry manag

2) Pt to increase his relaxation breathing to a daily (5 minutes, 1-2 times) exercise.
3) Pt to review handout on worry management and begin implement one of the recommended strategies.

4) f/u with IBHC in 2-3 weeks

ement strategies.

Other problem recommendations for PCM Team:

Engaging in cognitive behavioral strategies to manage his symptoms and change thought behaviors appears to be

Behavioral Health Consultation

Page 3 of 4
(Continued)

Result type:

Result date: July 09, 2018 13:06 PDT

Result status: Auth (Verified)

Result title: BHOP

Performed by: WILLIAMS, JEFFREY K on July 09, 2018 13:13 PDT
Veriﬁed by: WILLIAMS, JEFFREY K on July 11, 2018 14:35 PDT
Encounter info: 1608626, 0128C, Clinic, 07/09/2018 - 07/12/2018
Printed by: WlLLlAMS, JEFFREY K

Printed on: 07/12/18 09:10 PDT

  
 

appropriate treatment at this time. If symptoms remain elevated, omer treatment options may need to be explored.

signature Line
Electronically Signed on: 07/11/2018 02:35 PM PDT

WILLIAMS, JEFFREY K

Result type: Behavioral Health Consultation
Result date: July 09, 2018 13:06 PDT

Result status: Auth (Veriﬁed)

Result title: BHOP

Performed by: WILLIAMS, JEFFREY K on July 09, 2018 13:13 PDT
Veriﬁed by: WILLIAMS, JEFFREY K on July 11, 2018 14:35 PDT

Encounter info: 1608626, 0128C, Clinic, 07/09/2018 - 07/12/2018

Printed by: VVlLLlAMS, JEFFREY K
Printed on: 07/12/18 09:10 PDT

Page 4 of 4
(End of Report)

